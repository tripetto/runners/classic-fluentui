import { TStylesContract } from "@tripetto/runner";

declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

export default (pgettext: (context: string, id: string, ...args: string[]) => string): TStylesContract => ({
    contract: {
        name: PACKAGE_NAME,
        version: PACKAGE_VERSION,
    },
    styles: [
        {
            title: pgettext("runner:classic:fluentui", "Fluent UI Theme"),
            elements: [
                {
                    type: "color",
                    name: "color",
                    label: pgettext("runner:classic", "Primary color"),
                },
                {
                    type: "color",
                    name: "textColor",
                    label: pgettext("runner:classic", "Text color"),
                },
                {
                    type: "color",
                    name: "backgroundColor",
                    label: pgettext("runner:classic", "Background color"),
                },
            ],
        },
        {
            title: pgettext("runner:classic:fluentui", "Custom Fluent UI palette"),
            name: "palette",
            optional: true,
            elements: [
                {
                    type: "static",
                    label: pgettext(
                        "runner:classic:fluentui",
                        "Use the [Fluent UI Theme Designer](https://fluentuipr.z22.web.core.windows.net/heads/master/theming-designer/index.html) to generate a theme for the form and then copy-paste the palette colors to the fields below."
                    ),
                },
                { type: "color", name: "themePrimary", label: "themePrimary", default: "#0078d4" },
                { type: "color", name: "themeLighterAlt", label: "themeLighterAlt", default: "#eff6fc" },
                { type: "color", name: "themeLighter", label: "themeLighter", default: "#deecf9" },
                { type: "color", name: "themeLight", label: "themeLight", default: "#c7e0f4" },
                { type: "color", name: "themeTertiary", label: "themeTertiary", default: "#71afe5" },
                { type: "color", name: "themeSecondary", label: "themeSecondary", default: "#2b88d8" },
                { type: "color", name: "themeDarkAlt", label: "themeDarkAlt", default: "#106ebe" },
                { type: "color", name: "themeDark", label: "themeDark", default: "#005a9e" },
                { type: "color", name: "themeDarker", label: "themeDarker", default: "#004578" },
                { type: "color", name: "neutralLighterAlt", label: "neutralLighterAlt", default: "#faf9f8" },
                { type: "color", name: "neutralLighter", label: "neutralLighter", default: "#f3f2f1" },
                { type: "color", name: "neutralLight", label: "neutralLight", default: "#edebe9" },
                { type: "color", name: "neutralQuaternaryAlt", label: "neutralQuaternaryAlt", default: "#e1dfdd" },
                { type: "color", name: "neutralQuaternary", label: "neutralQuaternary", default: "#d0d0d0" },
                { type: "color", name: "neutralTertiaryAlt", label: "neutralTertiaryAlt", default: "#c8c6c4" },
                { type: "color", name: "neutralTertiary", label: "neutralTertiary", default: "#a19f9d" },
                { type: "color", name: "neutralSecondary", label: "neutralSecondary", default: "#605e5c" },
                { type: "color", name: "neutralSecondaryAlt", label: "neutralSecondaryAlt", default: "#8a8886" },
                { type: "color", name: "neutralPrimaryAlt", label: "neutralPrimaryAlt", default: "#3b3a39" },
                { type: "color", name: "neutralPrimary", label: "neutralPrimary", default: "#323130" },
                { type: "color", name: "neutralDark", label: "neutralDark", default: "#201f1e" },
                { type: "color", name: "black", label: "black", default: "#000000" },
                { type: "color", name: "white", label: "white", default: "#ffffff" },
            ],

            dependency: {
                observe: [
                    {
                        property: "color",
                        value: "",
                    },
                    {
                        property: "textColor",
                        value: "",
                    },
                    {
                        property: "backgroundColor",
                        value: "",
                    },
                ],
                action: "enable",
            },
        },
        {
            title: pgettext("runner:classic:fluentui", "Appearance"),
            additional: true,
            elements: [
                {
                    type: "radiobuttons",
                    name: "mode",
                    buttons: [
                        {
                            label: pgettext("runner:classic:fluentui", "Show entire form"),
                            value: "ahead",
                            description: pgettext("runner:classic:fluentui", "Present the entire form at once."),
                        },
                        {
                            label: pgettext("runner:classic:fluentui", "Progressive"),
                            value: "progressive",
                            description: pgettext(
                                "runner:classic:fluentui",
                                "Present the entire form till the point of any form validation error."
                            ),
                        },
                        {
                            label: pgettext("runner:classic:fluentui", "Continuous"),
                            value: "continuous",
                            description: pgettext(
                                "runner:classic:fluentui",
                                "Keep answered blocks in view while working through the form."
                            ),
                        },
                        {
                            label: pgettext("runner:classic:fluentui", "Paginated"),
                            value: "paginated",
                            description: pgettext(
                                "runner:classic:fluentui",
                                "Use pagination for the form and let the user navigate through the form using the next and back buttons."
                            ),
                        },
                    ],
                    default: "ahead",
                },
            ],
        },
        {
            title: pgettext("runner:classic:fluentui", "Options"),
            additional: true,
            elements: [
                {
                    type: "checkbox",
                    name: "autoFocus",
                    label: pgettext("runner:classic:fluentui", "Automatically gain focus"),
                    description: pgettext("runner:classic:fluentui", "Only applies when embedding the form."),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "showEnumerators",
                    label: pgettext("runner:classic:fluentui", "Display numbering"),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "showPageIndicators",
                    label: pgettext("runner:classic:fluentui", "Display page indicators"),
                    description: pgettext("runner:classic:fluentui", "Only available for paginated forms."),
                    default: false,
                    dependency: {
                        observe: {
                            property: "mode",
                            value: "paginated",
                        },
                        action: "enable",
                    },
                },
                {
                    type: "checkbox",
                    name: "showProgressbar",
                    label: pgettext("runner:classic:fluentui", "Display progressbar"),
                    description: pgettext("runner:classic:fluentui", "Only available for paginated forms."),
                    default: false,
                    dependency: {
                        observe: {
                            property: "mode",
                            value: "paginated",
                        },
                        action: "enable",
                    },
                },
                {
                    type: "checkbox",
                    name: "noBranding",
                    label: pgettext("runner:classic:fluentui", "Hide all the Tripetto branding"),
                    default: false,
                    tier: "licensed",
                },
            ],
        },
    ],
});
