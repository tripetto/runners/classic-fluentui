import { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";

export const FrameConfig = {
    shouldForwardProp: (prop: string) => {
        switch (prop) {
            case "children":
            case "frameRef":
            case "title":
            case "style":
            case "className":
            case "onTouch":
                return true;
        }

        return false;
    },
};

export const Frame = (props: {
    readonly children?: ((document: Document) => ReactNode) | ReactNode;
    readonly frameRef?: MutableRefObject<HTMLIFrameElement>;
    readonly title?: string;
    readonly style?: CSSProperties;
    readonly className?: string;
    readonly onTouch?: () => void;
}) => {
    const ref = props.frameRef || (useRef<HTMLIFrameElement>() as MutableRefObject<HTMLIFrameElement>);
    const [doc, setDoc] = useState<Document | undefined>();

    useEffect(() => {
        const frameElement = ref.current;
        const updateDoc = () => {
            if (!doc) {
                const contentDocument = frameElement.contentDocument;

                if (props.onTouch) {
                    const onTouch = props.onTouch;

                    frameElement.contentWindow?.addEventListener("mousedown", () => onTouch());
                    frameElement.contentWindow?.addEventListener("touchstart", () => onTouch());
                    frameElement.contentWindow?.addEventListener("wheel", () => onTouch());
                }

                if (contentDocument) {
                    setDoc(contentDocument);
                }
            }
        };

        if (!doc) {
            const contentDocument = frameElement.contentDocument;

            if (contentDocument && contentDocument.readyState === "complete") {
                return updateDoc();
            } else {
                frameElement.addEventListener("load", updateDoc);
            }
        }

        return () => {
            frameElement.removeEventListener("load", updateDoc);
        };
    }, []);

    return (
        <iframe ref={ref} title={props.title} style={props.style} className={props.className}>
            {doc && createPortal(typeof props.children === "function" ? props.children(doc) : props.children, doc.body)}
        </iframe>
    );
};
