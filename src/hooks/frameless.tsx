import { styled } from "styled-components";
import { CSSProperties, ReactNode } from "react";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { MARGIN, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "@ui/const";
import { Theme, ThemeProvider } from "@fluentui/react/lib/Theme";
import { IRuntimeStyles } from "@hooks/styles";

const RootElement = styled.div<{
    $view: TRunnerViews;
    $isPage: boolean;
    $styles: IRuntimeStyles;
}>`
    position: ${(props) => (props.$view === "live" && props.$isPage ? "fixed" : undefined)};
    left: ${(props) => (props.$view === "live" && props.$isPage ? 0 : undefined)};
    right: ${(props) => (props.$view === "live" && props.$isPage ? 0 : undefined)};
    top: ${(props) => (props.$view === "live" && props.$isPage ? 0 : undefined)};
    bottom: ${(props) => (props.$view === "live" && props.$isPage ? 0 : undefined)};
    width: ${(props) => (props.$view === "live" && props.$isPage ? undefined : "100%")};
    height: ${(props) => (props.$view === "live" ? undefined : "100%")};
    font-variant-ligatures: none;
    background-color: ${(props) => props.$styles.backgroundColor};
    overflow-x: ${(props) => (props.$view === "live" && !props.$isPage ? "visible" : "hidden")};
    overflow-y: ${(props) => (props.$view === "live" && !props.$isPage ? "visible" : "auto")};
    scroll-behavior: smooth;
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    > div {
        max-width: ${(props) => props.$isPage && "1140px"};
        margin-left: ${(props) => props.$isPage && "auto"};
        margin-right: ${(props) => props.$isPage && "auto"};
        padding: ${(props) => (props.$isPage || props.$view !== "live" ? `${MARGIN}px` : undefined)};
    }

    * {
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        > div {
            padding: ${(props) => (props.$isPage || props.$view !== "live" ? `${SMALL_SCREEN_MARGIN}px` : undefined)};
        }
    }
`;

export const Frameless = (props: {
    readonly children?: ReactNode;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly styles: IRuntimeStyles;
    readonly style?: CSSProperties;
    readonly theme: Theme;
    readonly className?: string;
    readonly onTouch?: () => void;
}) => (
    <RootElement
        style={props.style}
        className={props.className}
        onMouseDown={props.onTouch && (() => props.onTouch!())}
        onTouchStart={props.onTouch && (() => props.onTouch!())}
        onWheel={props.onTouch && (() => props.onTouch!())}
        $view={props.view}
        $isPage={props.isPage}
        $styles={props.styles}
    >
        <ThemeProvider theme={props.theme}>
            <div>{props.children}</div>
        </ThemeProvider>
    </RootElement>
);
