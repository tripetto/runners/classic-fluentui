import { IClassicStyles } from "@interfaces/styles";
import { TModes } from "@tripetto/runner";
import { useRef } from "react";
import { Theme, createTheme } from "@fluentui/react/lib/Theme";
import { BaseSlots, ThemeGenerator, themeRulesStandardCreator } from "@fluentui/react/lib/ThemeGenerator";
import { getColorFromString, isDark } from "@fluentui/react/lib/Color";

export interface IRuntimeStyles {
    readonly mode?: "paginated" | "continuous" | "progressive" | "ahead";
    readonly autoFocus?: boolean;
    readonly noBranding?: boolean;
    readonly showEnumerators?: boolean;
    readonly showPageIndicators?: boolean;
    readonly showProgressbar?: boolean;
    readonly theme: Theme;
    readonly backgroundColor?: string;
}

const generateRuntimeStyles = (styles: IClassicStyles, hasLicense: boolean, removeBranding: boolean): IRuntimeStyles => {
    if (!styles.palette) {
        const themeRules = themeRulesStandardCreator();
        const colors = {
            primaryColor: getColorFromString(styles.color || "#0078d4") || getColorFromString("#0078d4")!,
            textColor: getColorFromString(styles.textColor || "#323130") || getColorFromString("#323130")!,
            backgroundColor: getColorFromString(styles.backgroundColor || "#ffffff") || getColorFromString("#ffffff")!,
        };
        ThemeGenerator.insureSlots(themeRules, isDark(themeRules[BaseSlots[BaseSlots.backgroundColor]].color!));
        ThemeGenerator.setSlot(themeRules[BaseSlots[BaseSlots.primaryColor]], colors.primaryColor, undefined, false, true);
        ThemeGenerator.setSlot(themeRules[BaseSlots[BaseSlots.foregroundColor]], colors.textColor, undefined, false, true);
        ThemeGenerator.setSlot(themeRules[BaseSlots[BaseSlots.backgroundColor]], colors.backgroundColor, undefined, false, true);

        const palette: {
            [key: string]: string;
        } = ThemeGenerator.getThemeAsJson(themeRules);

        return {
            ...styles,
            backgroundColor: palette.white || undefined,
            theme: createTheme({
                palette,
                isInverted: isDark(themeRules[BaseSlots[BaseSlots.backgroundColor]].color!),
            }),
        };
    }

    return {
        ...styles,
        backgroundColor: (styles.palette?.white && getColorFromString(styles.palette.white)?.str) || undefined,
        theme: createTheme({
            palette: {
                themePrimary: (styles.palette?.themePrimary && getColorFromString(styles.palette.themePrimary)?.str) || undefined,
                themeLighterAlt: (styles.palette?.themeLighterAlt && getColorFromString(styles.palette.themeLighterAlt)?.str) || undefined,
                themeLighter: (styles.palette?.themeLighter && getColorFromString(styles.palette.themeLighter)?.str) || undefined,
                themeLight: (styles.palette?.themeLight && getColorFromString(styles.palette.themeLight)?.str) || undefined,
                themeTertiary: (styles.palette?.themeTertiary && getColorFromString(styles.palette.themeTertiary)?.str) || undefined,
                themeSecondary: (styles.palette?.themeSecondary && getColorFromString(styles.palette.themeSecondary)?.str) || undefined,
                themeDarkAlt: (styles.palette?.themeDarkAlt && getColorFromString(styles.palette.themeDarkAlt)?.str) || undefined,
                themeDark: (styles.palette?.themeDark && getColorFromString(styles.palette.themeDark)?.str) || undefined,
                themeDarker: (styles.palette?.themeDarker && getColorFromString(styles.palette.themeDarker)?.str) || undefined,
                neutralLighterAlt:
                    (styles.palette?.neutralLighterAlt && getColorFromString(styles.palette.neutralLighterAlt)?.str) || undefined,
                neutralLighter: (styles.palette?.neutralLighter && getColorFromString(styles.palette.neutralLighter)?.str) || undefined,
                neutralLight: (styles.palette?.neutralLight && getColorFromString(styles.palette.neutralLight)?.str) || undefined,
                neutralQuaternaryAlt:
                    (styles.palette?.neutralQuaternaryAlt && getColorFromString(styles.palette.neutralQuaternaryAlt)?.str) || undefined,
                neutralQuaternary:
                    (styles.palette?.neutralQuaternary && getColorFromString(styles.palette.neutralQuaternary)?.str) || undefined,
                neutralTertiaryAlt:
                    (styles.palette?.neutralTertiaryAlt && getColorFromString(styles.palette.neutralTertiaryAlt)?.str) || undefined,
                neutralTertiary: (styles.palette?.neutralTertiary && getColorFromString(styles.palette.neutralTertiary)?.str) || undefined,
                neutralSecondary:
                    (styles.palette?.neutralSecondary && getColorFromString(styles.palette.neutralSecondary)?.str) || undefined,
                neutralSecondaryAlt:
                    (styles.palette?.neutralSecondaryAlt && getColorFromString(styles.palette.neutralSecondaryAlt)?.str) || undefined,
                neutralPrimaryAlt:
                    (styles.palette?.neutralPrimaryAlt && getColorFromString(styles.palette.neutralPrimaryAlt)?.str) || undefined,
                neutralPrimary: (styles.palette?.neutralPrimary && getColorFromString(styles.palette.neutralPrimary)?.str) || undefined,
                neutralDark: (styles.palette?.neutralDark && getColorFromString(styles.palette.neutralDark)?.str) || undefined,
                black: (styles.palette?.black && getColorFromString(styles.palette.black)?.str) || undefined,
                white: (styles.palette?.white && getColorFromString(styles.palette.white)?.str) || undefined,
            },
        }),
        noBranding: (hasLicense && (removeBranding || styles.noBranding)) || false,
    };
};

export const useStyles = (
    initialStyles: IClassicStyles | undefined,
    hasLicense: boolean,
    removeBranding: boolean,
    onMode: (mode: TModes) => void,
    onChange: () => void
) => {
    const designtimeStyles = useRef<IClassicStyles>(initialStyles || {});
    const runtimeStyles = useRef<IRuntimeStyles>();

    if (!runtimeStyles.current) {
        runtimeStyles.current = generateRuntimeStyles(designtimeStyles.current, hasLicense, removeBranding);
    }

    return [
        designtimeStyles.current,
        runtimeStyles.current,
        (styles: IClassicStyles) => {
            designtimeStyles.current = styles;
            runtimeStyles.current = generateRuntimeStyles(styles, hasLicense, removeBranding);

            onMode(
                styles.mode === "paginated"
                    ? "paginated"
                    : styles.mode === "continuous"
                    ? "continuous"
                    : styles.mode === "progressive"
                    ? "progressive"
                    : "ahead"
            );
            onChange();
        },
    ] as [IClassicStyles, IRuntimeStyles, (styles: IClassicStyles) => void];
};
