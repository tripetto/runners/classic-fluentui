import { FocusEvent, MutableRefObject, useEffect, useRef, useState } from "react";
import { IObservableNode, TAny, extendImmutable, isNumberFinite } from "@tripetto/runner";
import { IClassicRendering } from "@interfaces/block";

export interface IFocus {
    [key: string]: boolean | undefined;
}

export const useFocus = (props: {
    readonly gainFocus?: boolean;
    readonly initialFocus?: IFocus;
    readonly onFocus?: () => void;
    readonly hooks?: {
        hook: (type: "restart", on: () => void) => void;
        onInteraction: () => void;
    };
    readonly page?: number;
}) => {
    const [focus, setFocus] = useState<IFocus>(props.initialFocus || {});
    const frameRef = useRef<HTMLIFrameElement>();
    const gainRef = useRef(true);
    const pageRef = useRef<number>();
    const autoFocusRef = useRef<string>();
    const elementsRef = useRef<{
        [key: string]: HTMLElement | undefined;
    }>({});

    if (pageRef.current !== props.page) {
        pageRef.current = props.page;
        gainRef.current = true;
    }

    useEffect(() => {
        if (gainRef.current && autoFocusRef.current) {
            const focusElement = elementsRef.current[autoFocusRef.current];

            if (focusElement) {
                if (document.hasFocus()) {
                    const activeElement = document.activeElement;
                    let allowFocus = activeElement && frameRef.current && activeElement.isEqualNode(frameRef.current);

                    if (props.gainFocus) {
                        allowFocus = allowFocus || !activeElement || activeElement.tagName === "BODY";
                    }

                    gainRef.current = false;

                    if (allowFocus) {
                        const findFocusableElement = (root: HTMLElement): HTMLElement | undefined => {
                            if (isNumberFinite(root.tabIndex) && root.tabIndex > 0) {
                                return root;
                            }

                            let child = root.firstElementChild;

                            while (child) {
                                const element = findFocusableElement(child as HTMLElement);

                                if (element) {
                                    return element;
                                }

                                child = child.nextElementSibling;
                            }

                            return undefined;
                        };

                        findFocusableElement(focusElement)?.focus({
                            preventScroll: true,
                        });
                    }
                }

                delete elementsRef.current[autoFocusRef.current];
            }
        }
    });

    return [
        frameRef as MutableRefObject<HTMLIFrameElement>,
        () => setFocus({}),
        (ref: IObservableNode<IClassicRendering>) => focus[ref.key],
        (ref: IObservableNode<IClassicRendering>, hasFocus: boolean, on?: () => void) => () => {
            if (hasFocus) {
                gainRef.current = false;
            }

            setFocus(
                extendImmutable(focus, {
                    [ref.key]: hasFocus,
                })
            );

            if (hasFocus && props.onFocus) {
                props.onFocus();
            }

            if (on && focus[ref.key] !== hasFocus) {
                on();
            }
        },
        (ref: IObservableNode<IClassicRendering>) => (el: HTMLElement | null) => {
            if (el) {
                elementsRef.current[ref.key] = el;

                if (!autoFocusRef.current) {
                    autoFocusRef.current = ref.key;
                }
            } else {
                if (autoFocusRef.current === ref.key) {
                    autoFocusRef.current = undefined;
                }

                delete elementsRef.current[ref.key];
            }
        },
        focus,
    ] as [
        MutableRefObject<HTMLIFrameElement>,
        () => void,
        (ref: IObservableNode<IClassicRendering>) => boolean | undefined,
        (ref: IObservableNode<IClassicRendering>, hasFocus: boolean, on?: () => void) => (e: FocusEvent) => TAny,
        (ref: IObservableNode<IClassicRendering>) => (el: HTMLElement | null) => void,
        IFocus,
    ];
};
