import { FocusEvent } from "react";
import { TSerializeTypes, arrayItem, castToString, findFirst, isFunction, isString } from "@tripetto/runner";
import { Dropdown } from "@fluentui/react/lib/Dropdown";

export interface IDropdownOption {
    readonly id: string;
    readonly name: string;
    readonly value?: string;
}

export const DropdownControl = (props: {
    readonly options: IDropdownOption[];
    readonly value: {
        reference?: string;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly set: (value: TSerializeTypes, reference?: string) => void;
        readonly default: (value: TSerializeTypes, reference?: string) => void;
    };
    readonly id?: string;
    readonly label?: string | ((type: "field") => JSX.Element | undefined);
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLElement | null) => void;
    readonly onSubmit?: () => void;
}) => (
    <Dropdown
        id={props.id}
        ref={props.onAutoFocus}
        tabIndex={props.tabIndex}
        disabled={props.value.isFrozen || props.value.isLocked || false}
        label={(isString(props.label) && props.label) || undefined}
        onRenderLabel={() => (isFunction(props.label) && props.label("field")) || null}
        placeholder={props.placeholder}
        options={props.options
            .filter((option) => option.name && option.id)
            .map((option) => ({
                key: option.id,
                text: option.name,
            }))}
        defaultSelectedKey={assertValue(props.value, props, props.value.reference)}
        required={props.required}
        aria-describedby={props.ariaDescribedBy}
        onChange={(e, option) => {
            assertValue(props.value, props, (option && castToString(option.key)) || "");
        }}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        onKeyDown={(e) => {
            if (e.key === "Escape") {
                e.currentTarget.blur();
            } else if (e.key === "Tab" && !e.shiftKey && props.onSubmit) {
                e.preventDefault();

                props.onSubmit();
            }
        }}
    />
);

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string) => void;
        readonly default: (value: TSerializeTypes, reference?: string) => void;
    },
    props: {
        readonly options: IDropdownOption[];
        readonly placeholder?: string;
    },
    reference?: string
) => {
    const selected = findFirst(props.options, (option) => option.id === reference);

    if (!selected && !props.placeholder && props.options.length > 0) {
        const defaultOption = arrayItem(props.options, 0)!;

        valueRef.default(defaultOption.value || defaultOption.name, defaultOption.id);
    } else if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id);
    }

    return (selected && selected.id) || "";
};
