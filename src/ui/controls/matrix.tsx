import { styled } from "styled-components";
import { FocusEvent, useEffect, useRef, useState } from "react";
import { TSerializeTypes, cancelUITimeout, castToString, each, findFirst, scheduleUITimeout } from "@tripetto/runner";
import { useStyles } from "./styles";
import { ChoiceGroup } from "@fluentui/react/lib/ChoiceGroup";
import { Text } from "@fluentui/react/lib/Text";
import { Label } from "@fluentui/react/lib/Label";

const DEBOUNCE_NORMAL = 1000 / 20;
let MatrixCount = 0;

export interface IMatrixColumn {
    readonly id: string;
    readonly name: string;
    readonly label?: string | JSX.Element;
    readonly value?: string;
}

export interface IMatrixRow {
    readonly id: string;
    readonly label: string | JSX.Element;
    readonly alias?: string;
    readonly explanation?: string | JSX.Element;
    readonly required?: boolean;
    readonly tabIndex?: number;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string) => void;
          };
    readonly onChange?: (id: string) => void;
}

const MatrixContainerElement = styled.div`
    overflow-x: auto;
    scroll-behavior: smooth;
    scrollbar-width: none;
    -ms-overflow-style: none;
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    &::-webkit-scrollbar {
        display: none;
    }
`;

const MatrixElement = styled.table`
    display: table;
    width: 100%;
    border-collapse: collapse;

    > thead > tr {
        > th {
            text-align: center;
            vertical-align: bottom;
            font-weight: normal;
            cursor: default;
            padding-top: 8px;
            padding-bottom: 8px;

            &:first-child {
                background-color: transparent;
            }
        }

        > th + th {
            padding-left: 8px;
            padding-right: 8px;
        }
    }

    > tbody {
        > tr {
            > th {
                text-align: left;
                font-weight: normal;
                padding-right: 8px;
                padding-top: 8px;
                padding-bottom: 8px;
                cursor: default;
                min-width: 50%;

                > small {
                    opacity: 0.8;
                }
            }

            > td {
                position: relative;
                top: -3px;
                left: 3px;

                > * {
                    display: flex;
                    justify-content: center;
                }
            }
        }

        > tr:last-child {
            > th,
            > td {
                border-bottom: none;
            }
        }
    }
`;

export const MatrixControl = (props: {
    readonly columns: IMatrixColumn[];
    readonly rows: IMatrixRow[];
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly allowUnselect?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly required?: boolean;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const debounceRef = useRef<number>(0);
    const rowsRef = useRef<{
        [row: string]: string;
    }>({});
    const [, update] = useState({});
    const prefix = `${MatrixCount++}-`;
    const classes = useStyles((theme) => ({
        columns: {
            backgroundColor: theme.semanticColors.infoBackground,
            color: theme.semanticColors.bodyText,
        },
        rows: {
            borderBottom: `1px solid ${theme.semanticColors.infoBackground}`,
        },
    }));

    const changeValue = (row: IMatrixRow, column: IMatrixColumn | undefined) => {
        if (typeof row.value === "object") {
            cancelUITimeout(debounceRef.current);

            rowsRef.current[row.id] = column ? column.id : "";

            update({});

            debounceRef.current = scheduleUITimeout(() => {
                debounceRef.current = 0;

                each(
                    rowsRef.current,
                    (columnId, rowId: string) => {
                        const changedRow = findFirst(props.rows, (ref) => ref.id === rowId);

                        delete rowsRef.current[rowId];

                        if (changedRow && typeof changedRow.value === "object") {
                            const selectedColumn = (columnId && findFirst(props.columns, (ref) => ref.id === columnId)) || undefined;

                            changedRow.value.set(
                                selectedColumn && (selectedColumn.value || selectedColumn.name),
                                selectedColumn && selectedColumn.id
                            );
                        }
                    },
                    {
                        keys: true,
                    }
                );
            }, DEBOUNCE_NORMAL);
        }

        if (row.onChange) {
            row.onChange((column && column.id) || "");
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    return (
        <MatrixContainerElement>
            <MatrixElement>
                <thead>
                    <tr className={classes.rows}>
                        <th scope="col" />
                        {props.columns.map((column, index) => (
                            <th key={index} scope="col" className={classes.columns}>
                                <Text>{column.label || column.name || "..."}</Text>
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {props.rows.map((row, rowIndex) => (
                        <tr key={rowIndex} className={classes.rows}>
                            <th scope="row">
                                {row.required ? (
                                    <Label required={true}>{row.label || "..."}</Label>
                                ) : (
                                    <Text block={true}>{row.label || "..."}</Text>
                                )}
                                {row.explanation && (
                                    <Text variant="xSmall" block={true}>
                                        {row.explanation}
                                    </Text>
                                )}
                            </th>
                            {props.columns.map((column: IMatrixColumn, columnIndex: number) => {
                                const key = rowIndex + "-" + columnIndex;
                                const selected =
                                    (typeof row.value === "object"
                                        ? castToString(rowsRef.current[row.id], row.value.reference || "")
                                        : row.value) === column.id;

                                return (
                                    <td key={key}>
                                        <ChoiceGroup
                                            key={prefix + key}
                                            disabled={
                                                (typeof row.value === "object" && (row.value.isFrozen || row.value.isLocked)) || false
                                            }
                                            options={[
                                                {
                                                    key: key,
                                                    text: "",
                                                },
                                            ]}
                                            onChange={() => changeValue(row, column)}
                                            selectedKey={selected ? key : undefined}
                                            onClick={() => {
                                                if (props.allowUnselect && selected) {
                                                    changeValue(row, undefined);
                                                }
                                            }}
                                        />
                                    </td>
                                );
                            })}
                        </tr>
                    ))}
                </tbody>
            </MatrixElement>
        </MatrixContainerElement>
    );
};
