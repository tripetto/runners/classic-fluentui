import { FocusEvent } from "react";
import { TSerializeTypes, castToString, findFirst } from "@tripetto/runner";
import { ChoiceGroup } from "@fluentui/react/lib/ChoiceGroup";

export interface IRadiobutton {
    readonly id: string;
    readonly name: string;
    readonly label?: string | JSX.Element;
    readonly description?: string | JSX.Element;
    readonly value?: string;
}

export const RadiobuttonsControl = (props: {
    readonly buttons: IRadiobutton[];
    readonly value: {
        reference?: string;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly set: (value: TSerializeTypes, reference?: string) => void;
        readonly default: (value: TSerializeTypes, reference?: string) => void;
    };
    readonly id?: string;
    readonly required?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLElement | null) => void;
    readonly onSubmit?: () => void;
}) => (
    <ChoiceGroup
        id={props.id}
        ref={props.onAutoFocus}
        tabIndex={props.tabIndex}
        disabled={props.value.isFrozen || props.value.isLocked || false}
        options={props.buttons
            .filter((button) => button.id && (button.label || button.name || button.description))
            .map((button) => ({
                key: button.id,
                text: button.name,
            }))}
        defaultSelectedKey={assertValue(props.value, props, props.value.reference)}
        aria-describedby={props.ariaDescribedBy}
        onChange={(e, button) => {
            assertValue(props.value, props, (button && castToString(button.key)) || "");
        }}
        onFocus={props.onFocus}
        onBlur={props.onBlur}
        onKeyDown={(e) => {
            if (e.key === "Escape") {
                e.currentTarget.blur();
            } else if (e.key === "Tab" && !e.shiftKey && props.onSubmit) {
                e.preventDefault();

                props.onSubmit();
            }
        }}
    />
);

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string) => void;
    },
    props: {
        readonly buttons: IRadiobutton[];
    },
    reference?: string
) => {
    const selected = findFirst(props.buttons, (radiobutton) => radiobutton.id === reference);

    if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id);
    }

    return (selected && selected.id) || "";
};
