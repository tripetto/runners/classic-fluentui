import { FocusEvent } from "react";
import { isFunction, isString } from "@tripetto/runner";
import { Checkbox } from "@fluentui/react/lib/Checkbox";
import { Text } from "@fluentui/react/lib/Text";

export const CheckboxControl = (props: {
    readonly value: {
        value: boolean;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly confirm: () => void;
    };
    readonly label?: string | ((type: "checkbox") => JSX.Element | undefined);
    readonly required?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly tabSubmit?: boolean;
    readonly ariaDescribedBy?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
}) => {
    const renderLabel = (isFunction(props.label) && props.label) || undefined;

    props.value.confirm();

    return (
        <Checkbox
            ref={props.onAutoFocus}
            label={(isString(props.label) && props.label) || undefined}
            onRenderLabel={
                (renderLabel &&
                    (() => (
                        <Text
                            style={{
                                marginLeft: "4px",
                                lineHeight: "20px",
                            }}
                        >
                            {renderLabel("checkbox")}
                        </Text>
                    ))) ||
                undefined
            }
            checked={props.value.value}
            aria-describedby={props.ariaDescribedBy}
            disabled={props.value.isFrozen || props.value.isLocked || false}
            required={props.required}
            onChange={(e, checked?: boolean) => {
                props.value.value = checked || false;
            }}
            inputProps={{
                tabIndex: props.tabIndex,
                onFocus: props.onFocus,
                onBlur: props.onBlur,
                onKeyDown: (e) => {
                    if (e.key === "Enter") {
                        e.preventDefault();

                        if (e.shiftKey && props.onSubmit) {
                            props.onSubmit();
                        } else {
                            props.value.value = !props.value.value;
                        }
                    } else if (e.key === "Escape") {
                        e.currentTarget.blur();
                    } else if (
                        e.key === "Tab" &&
                        !e.shiftKey &&
                        (props.tabSubmit || typeof props.tabSubmit !== "boolean") &&
                        props.onSubmit
                    ) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                },
            }}
        />
    );
};
