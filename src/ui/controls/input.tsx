import { FocusEvent, useEffect, useRef, useState } from "react";
import { DateTime, Num, TSerializeTypes, cancelUITimeout, castToString, isFunction, isString, scheduleUITimeout } from "@tripetto/runner";
import { handleBlur, handleFocus, setReturnValue } from "./helpers";
import { TextField } from "@fluentui/react/lib/TextField";

const DEBOUNCE_MIN = 1000 / 60;
const DEBOUNCE_MAX = 1000;

export const InputControl = (props: {
    readonly type: "text" | "email" | "url" | "password" | "tel" | "number";
    readonly id?: string;
    readonly label?: string | ((type: "field") => JSX.Element | undefined);
    readonly placeholder?: string;
    readonly explanation?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly multiline?: boolean;
    readonly tabIndex?: number;
    readonly maxLength?: number;
    readonly value?:
        | string
        | {
              pristine: TSerializeTypes;
              readonly string: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
          };
    readonly ariaDescribedBy?: string;

    /**
     * https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/inputmode
     */
    readonly inputMode?: "text" | "tel" | "url" | "email" | "numeric" | "decimal" | "search";

    /**
     * https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete
     */
    readonly autoComplete?: string;
    readonly list?: string;
    readonly onChange?: (value: string) => string | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<{
        duration: number;
        handle: number;
        update?: () => void;
    }>({
        duration: 0,
        handle: 0,
    });
    const [focusValue, setFocusValue] = useState(typeof valueRef === "object" ? castToString(valueRef.pristine) : valueRef || "");
    const [focus, setFocusState] = useState(false);
    const setFocus = (hasFocus: boolean) => {
        if (hasFocus) {
            setFocusValue(typeof valueRef === "object" ? castToString(valueRef.pristine) : valueRef || "");
        }

        setFocusState(hasFocus);
    };

    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  focus ? focusValue : valueRef.string,
                  (val: string) => {
                      cancelUITimeout(debounceRef.current.handle);

                      setFocusValue(val);

                      debounceRef.current.handle = scheduleUITimeout(
                          () => {
                              const start = DateTime.precise;

                              debounceRef.current.handle = 0;
                              debounceRef.current.update = () => {
                                  debounceRef.current.duration = DateTime.elapsed(start, true);
                                  debounceRef.current.update = undefined;
                              };

                              if (props.type === "url" && val && val.toLowerCase().indexOf("://") === -1) {
                                  valueRef.pristine = `https://${val}`;
                              } else {
                                  valueRef.pristine = val || undefined;
                              }
                          },
                          Num.range(debounceRef.current.duration * 2, DEBOUNCE_MIN, DEBOUNCE_MAX)
                      );
                  },
              ]
            : [focusValue, setFocusValue];

    const ref = useRef<HTMLInputElement | null>();
    const setRef = (el: HTMLInputElement | null) => {
        if (props.onAutoFocus) {
            props.onAutoFocus(el);
        }

        ref.current = el;
    };

    useEffect(() => {
        if (ref.current && !focus) {
            ref.current.value = value;
        }
    }, [value]);

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current.handle);
        };
    }, []);

    if (debounceRef.current.update) {
        debounceRef.current.update();
    }

    return (
        <TextField
            id={props.id}
            elementRef={setRef}
            multiline={props.multiline || false}
            autoAdjustHeight={props.multiline || false}
            resizable={false}
            type={props.type || "text"}
            list={props.list}
            tabIndex={props.tabIndex}
            label={(isString(props.label) && props.label) || undefined}
            onRenderLabel={() => (isFunction(props.label) && props.label("field")) || null}
            placeholder={props.placeholder}
            description={props.explanation}
            required={props.required || false}
            disabled={props.disabled || false}
            readOnly={props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false}
            invalid={props.error || false}
            maxLength={props.maxLength || undefined}
            value={value}
            name={props.autoComplete}
            autoComplete={props.autoComplete || "off"}
            inputMode={props.inputMode || "text"}
            aria-describedby={props.ariaDescribedBy}
            aria-labelledby={isFunction(props.label) ? `${props.id}-label` : undefined}
            onChange={(e, v?: string) => {
                setValue(v || "");

                if (props.onChange) {
                    setReturnValue(setValue, props.onChange(v || ""));
                }
            }}
            onFocus={handleFocus(setFocus, setValue, props.onFocus)}
            onBlur={handleBlur(setFocus, setValue, props.onBlur)}
            onKeyDown={(e) => {
                if (e.key === "Enter" && (!props.multiline || e.shiftKey) && props.onSubmit) {
                    e.preventDefault();

                    props.onSubmit();
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                }
            }}
        />
    );
};
