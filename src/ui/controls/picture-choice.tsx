import { styled } from "styled-components";
import { FocusEvent, KeyboardEvent, MouseEvent, useEffect, useRef, useState } from "react";
import { TSerializeTypes, arrayItem, cancelUITimeout, castToBoolean, each, findFirst, scheduleUITimeout } from "@tripetto/runner";
import { handleAutoSubmit } from "./helpers";
import { useStyles } from "./styles";
import { Stack } from "@fluentui/react/lib/Stack";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";

const DEBOUNCE_NORMAL = 1000 / 20;

export interface IPictureChoiceOption {
    readonly id: string;
    readonly image?: string;
    readonly emoji?: string;
    readonly name?: string;
    readonly nameVisible?: boolean;
    readonly color?: string;
    readonly value?: string;
    readonly description?: JSX.Element;
    readonly url?: string;
    readonly target?: "self" | "blank";
    readonly disabled?: boolean;
    readonly slot?: {
        value: boolean;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
        readonly confirm: () => void;
    };
    readonly tabIndex?: number;
    readonly onChange?: (selected: boolean) => void;
}

const PictureChoiceElement = styled.button<{
    $hasLabel: boolean;
    $size: "small" | "medium" | "large";
    $color?: string;
}>`
    appearance: none;
    outline: none;
    user-select: none;
    box-sizing: border-box;
    text-align: left;
    border-radius: 2px;
    width: ${(props) => `calc(${props.$size === "small" ? 75 : props.$size === "large" ? 300 : 150}px + 16px)`};
    overflow: hidden;
    padding: 8px;
    margin: 0;
    opacity: 0.4;
    background-color: ${(props) => props.$color};

    @media (max-device-width: 500px) {
        width: ${(props) => `calc(${props.$size === "small" ? 75 : props.$size === "large" ? 280 : 120}px + 16px)`};
    }

    @media (max-width: 500px) {
        width: ${(props) => `calc(${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px + 16px)`};
    }

    > div {
        display: flex;
        width: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 300 : 150}px`};
        height: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 300 : 150}px`};
        min-width: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        justify-content: center;
        align-items: center;
        overflow: hidden;
        white-space: nowrap;

        > span {
            font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 160 : 80}px`};
        }

        > i {
            font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 160 : 80}px`};
        }

        @media (max-device-width: 500px) {
            width: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};
            height: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};

            > span {
                font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 136 : 68}px`};
            }
        }

        @media (max-width: 500px) {
            width: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};
            height: ${(props) => `${props.$size === "small" ? 75 : props.$size === "large" ? 240 : 120}px`};

            > span {
                font-size: ${(props) => `${props.$size === "small" ? 40 : props.$size === "large" ? 136 : 68}px`};
            }
        }
    }

    > div + span {
        display: flex;
        justify-content: center;
        flex-direction: column;
        margin-top: 8px;
        text-align: center;
    }

    &:not(:disabled) {
        cursor: pointer;
        opacity: 1;

        &:focus {
            outline-offset: -3px;
            outline-width: 1px;
            outline-style: solid;
        }
    }
`;

const assertValue = (
    valueRef: {
        reference?: string;
        readonly set: (value: TSerializeTypes, reference?: string) => void;
    },
    options: IPictureChoiceOption[],
    reference?: string
) => {
    const selected = findFirst(options, (option) => option.id === reference);

    if (valueRef.reference !== selected?.id) {
        valueRef.set(selected && (selected.value || selected.name), selected?.id);
    }

    return (selected && selected.id) || "";
};

export const PictureChoiceControl = (props: {
    readonly options: IPictureChoiceOption[];
    readonly size?: "small" | "medium" | "large";
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly ariaDescribedBy?: string;
    readonly tabIndex?: number;
    readonly value?:
        | string
        | {
              reference?: string;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly set: (value: TSerializeTypes, reference?: string) => void;
          };
    readonly autoSubmit?: boolean;
    readonly view?: "live" | "test" | "preview";
    readonly onChange?: (value: string) => void;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLButtonElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<number>(0);
    const optionsRef = useRef<{
        [option: string]: boolean;
    }>({});
    const [proxy, setProxy] = useState((typeof valueRef !== "object" && valueRef) || "");
    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  debounceRef.current !== 0 ? proxy : assertValue(valueRef, props.options, valueRef.reference),
                  (reference: string) => {
                      cancelUITimeout(debounceRef.current);

                      setProxy(reference);

                      debounceRef.current = scheduleUITimeout(() => {
                          debounceRef.current = 0;

                          assertValue(valueRef, props.options, reference);

                          if (props.autoSubmit && reference) {
                              handleAutoSubmit(autoSubmit);
                          }
                      }, DEBOUNCE_NORMAL);
                  },
              ]
            : [proxy, setProxy];
    const [, update] = useState({});
    const autoSubmit = useRef({
        id: 0,
        cb: props.onSubmit,
    });
    const classes = useStyles((theme) => ({
        unselected: {
            background: theme.semanticColors.buttonBackground,
            border: `1px solid ${theme.semanticColors.buttonBorder}`,
            color: theme.semanticColors.buttonText,

            selectors: {
                ":hover": {
                    background: theme.semanticColors.buttonBackgroundHovered,
                },
            },
        },
        selected: {
            background: theme.semanticColors.primaryButtonBackground,
            border: `1px solid ${theme.semanticColors.primaryButtonBackground}`,
            color: theme.semanticColors.primaryButtonText,

            selectors: {
                ":hover": {
                    background: theme.semanticColors.primaryButtonBackgroundHovered,
                    border: `1px solid ${theme.semanticColors.primaryButtonBackgroundHovered}`,
                },
            },
        },
        labelUnselected: {
            color: theme.semanticColors.buttonText,
        },
        labelSelected: {
            color: theme.semanticColors.primaryButtonText,
        },
    }));

    const toggle = (option: IPictureChoiceOption) => {
        if (option.url) {
            return;
        }

        const isSelected = option.slot ? castToBoolean(optionsRef.current[option.id], option.slot.value) : value === option.id;

        if (option.slot) {
            cancelUITimeout(debounceRef.current);

            optionsRef.current[option.id] = !isSelected;

            update({});

            debounceRef.current = scheduleUITimeout(() => {
                debounceRef.current = 0;

                each(
                    optionsRef.current,
                    (val, id: string) => {
                        const changedOption = findFirst(props.options, (ref) => ref.id === id);

                        delete optionsRef.current[id];

                        if (changedOption && changedOption.slot) {
                            changedOption.slot.value = val;
                        }
                    },
                    {
                        keys: true,
                    }
                );
            }, DEBOUNCE_NORMAL);
        } else {
            const val = isSelected && !props.required ? "" : option.id;

            if (autoSubmit.current.id) {
                clearTimeout(autoSubmit.current.id);

                autoSubmit.current.id = 0;
            }

            setValue(val);

            if (props.onChange) {
                props.onChange(val);
            }

            if (typeof valueRef !== "object" && props.autoSubmit && val) {
                handleAutoSubmit(autoSubmit);
            }
        }

        if (option.onChange) {
            option.onChange(!option.slot && props.required ? true : !isSelected);
        }
    };

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current);
        };
    }, []);

    autoSubmit.current.cb = props.onSubmit;

    return (
        <Stack horizontal wrap tokens={{ childrenGap: 12 }}>
            {props.options.map((option, index) => {
                const isSelected = option.slot ? castToBoolean(optionsRef.current[option.id], option.slot.value) : value === option.id;
                const label = (option.nameVisible && option.name) || undefined;

                if (option.slot) {
                    option.slot.confirm();
                }

                return (
                    (props.view === "preview" || option.image || option.emoji || label || option.description) && (
                        <Stack.Item
                            key={option.id || index}
                            style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "stretch",
                            }}
                        >
                            <PictureChoiceElement
                                className={isSelected ? classes.selected : classes.unselected}
                                ref={((option.slot || !value ? index === 0 : isSelected) && props.onAutoFocus) || undefined}
                                type="button"
                                disabled={
                                    option.disabled ||
                                    props.disabled ||
                                    props.readOnly ||
                                    (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) ||
                                    (option.slot && (option.slot.isFrozen || option.slot.isLocked)) ||
                                    false
                                }
                                tabIndex={props.tabIndex}
                                aria-describedby={props.ariaDescribedBy}
                                onFocus={props.onFocus}
                                onBlur={props.onBlur}
                                onKeyDown={(e: KeyboardEvent<HTMLButtonElement>) => {
                                    if (e.shiftKey && e.key === "Enter" && props.onSubmit) {
                                        e.preventDefault();

                                        props.onSubmit();
                                    } else if (e.key === "Escape") {
                                        e.currentTarget.blur();
                                    } else if (e.key === "Tab") {
                                        if (e.shiftKey) {
                                            if (props.onCancel && index === 0) {
                                                e.preventDefault();

                                                props.onCancel();
                                            }
                                        } else if (props.onSubmit && index + 1 === props.options.length) {
                                            e.preventDefault();

                                            props.onSubmit();
                                        }
                                    } else {
                                        const keyCode = (e.key.length === 1 && e.key.charCodeAt(0)) || 0;
                                        const offset =
                                            (keyCode <= 57 ? keyCode - 48 : 0) ||
                                            (keyCode <= 90 ? keyCode - 64 : 0) ||
                                            (keyCode <= 122 ? keyCode - 96 : 0);

                                        if (offset > 0 && offset <= 26) {
                                            const toggleOption = arrayItem(props.options, offset - 1);

                                            if (toggleOption) {
                                                toggle(toggleOption);
                                            }
                                        }
                                    }
                                }}
                                onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                    e.stopPropagation();

                                    if (option.url) {
                                        window.open(option.url, `_${option.target || "blank"}`, "noopener");
                                    } else {
                                        toggle(option);
                                    }
                                }}
                                $hasLabel={label ? true : false}
                                $size={props.size || "medium"}
                                $color={(isSelected && option.color) || undefined}
                            >
                                <div
                                    style={{
                                        backgroundImage: (option.image && `url("${option.image}")`) || undefined,
                                    }}
                                >
                                    {option.emoji && <span>{option.emoji}</span>}
                                    {!option.image && !option.emoji && (
                                        <FontIcon
                                            iconName="FileImage"
                                            className={isSelected ? classes.labelSelected : classes.labelUnselected}
                                        />
                                    )}
                                </div>
                                {label && (
                                    <span>
                                        <Text block={true} className={isSelected ? classes.labelSelected : classes.labelUnselected}>
                                            {label}
                                        </Text>
                                        {option.description && (
                                            <Text
                                                block={true}
                                                variant="xSmall"
                                                className={isSelected ? classes.labelSelected : classes.labelUnselected}
                                            >
                                                {option.description}
                                            </Text>
                                        )}
                                    </span>
                                )}
                            </PictureChoiceElement>
                        </Stack.Item>
                    )
                );
            })}
        </Stack>
    );
};
