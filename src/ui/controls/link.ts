import { styled } from "styled-components";

export const LinkElement = styled.a`
    text-decoration: underline;
    color: inherit !important;
`;
