import { styled } from "styled-components";
import { DragEvent, FocusEvent, KeyboardEvent, ReactNode, useEffect, useRef, useState } from "react";
import { Num, Str } from "@tripetto/runner";
import { useStyles } from "./styles";
import { DefaultButton, PrimaryButton } from "@fluentui/react/lib/Button";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";
import { ProgressIndicator } from "@fluentui/react/lib/ProgressIndicator";
import { Spinner, SpinnerSize } from "@fluentui/react/lib/Spinner";

export interface IFileController {
    readonly isImage: boolean;
    readonly limit: number;
    readonly allowedExtensions: string[];
    readonly upload: (files: FileList, service: IFileService | undefined, onProgress: (percent: number) => void) => Promise<void>;
    readonly download: (service?: IFileService) => Promise<string>;
    readonly delete: (service?: IFileService) => Promise<void>;
    readonly fileSlot: {
        readonly hasValue: boolean;
        readonly string: string;
        readonly reference: string | undefined;
        readonly isLocked: boolean;
        readonly isFrozen: boolean;
    };
}

export interface IFileService {
    readonly get: (file: string) => Promise<Blob>;
    readonly put: (file: File, onProgress?: (percentage: number) => void) => Promise<string>;
    readonly delete: (file: string) => Promise<void>;
}

const FileElement = styled.div<{
    $disabled: boolean;
}>`
    outline: none;
    box-sizing: border-box;
    display: block;
    width: 100%;
    height: 184px;
    position: relative;
    overflow: hidden;
    border-radius: 2px;
    padding: 16px;
    margin: 0;
    opacity: ${(props) => (props.$disabled ? 0.4 : 1)};
    transition:
        color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out,
        opacity 0.15s ease-in-out;
    cursor: default;

    > div,
    > label {
        position: absolute;
        left: 12px;
        right: 12px;
        top: 12px;
        bottom: 12px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
`;

const FileBrowseElement = styled.label`
    cursor: pointer;

    * {
        pointer-events: none;
    }

    > input {
        width: 1px;
        height: 1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
`;

const FileErrorElement = styled.div`
    > span {
        text-align: center;

        &:first-child {
            font-weight: bold;
        }
    }

    > button {
        margin-top: 8px;
    }
`;

const FilePreviewElement = styled.div`
    > img {
        display: block;
        height: 80px;
        margin: 8px;
        margin-top: 0;
    }

    > i {
        margin-bottom: 8px;
    }

    > button {
        margin-top: 8px;
    }

    > div {
        margin-top: 20px;
    }
`;

export const FileThumbnailControl = (props: {
    readonly controller: IFileController;
    readonly service?: IFileService;
    readonly host?: (props: { children: ReactNode }) => JSX.Element;
    readonly error?: ReactNode;
    readonly loading?: ReactNode;
}) => {
    const [data, setData] = useState({
        loading: props.controller.isImage,
        base64data: "",
    });

    useEffect(() => {
        if (props.controller.isImage) {
            if (!data.loading) {
                setData({
                    loading: true,
                    base64data: "",
                });
            }

            props.controller
                .download(props.service)
                .then((base64data) =>
                    setData({
                        loading: false,
                        base64data,
                    })
                )
                .catch(() =>
                    setData({
                        loading: false,
                        base64data: "",
                    })
                );
        }
    }, [props.controller.fileSlot.reference]);

    if (data.loading) {
        return <>{props.loading}</>;
    }

    return (
        (data.base64data && (props.host ? <props.host children={<img src={data.base64data} />} /> : <img src={data.base64data} />)) || (
            <>{props.error}</>
        )
    );
};

export const FileControl = (props: {
    readonly controller: IFileController;
    readonly labels: (
        id:
            | "browse"
            | "explanation"
            | "dragging"
            | "limit"
            | "extensions"
            | "retry"
            | "progress"
            | "delete"
            | "invalid-file"
            | "invalid-amount"
            | "invalid-extension"
            | "invalid-size"
            | "error",
        message: string
    ) => string;
    readonly service?: IFileService;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLDivElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const [dragging, setDragging] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [progress, setProgress] = useState(-1);
    const [error, setError] = useState<"invalid-amount" | "invalid-extension" | "invalid-size" | string>("");
    const inputRef = useRef<HTMLInputElement | null>();
    const disabled = props.disabled || props.controller.fileSlot.isFrozen || props.controller.fileSlot.isLocked || false;
    const classes = useStyles((theme) => ({
        root: {
            background: theme.semanticColors.buttonBackground,
            border: `1px solid ${theme.semanticColors.buttonBorder}`,
            color: theme.semanticColors.buttonText,

            selectors: {
                ":hover": {
                    background: theme.semanticColors.buttonBackgroundHovered,
                },
            },
        },
        icon: {
            color: theme.semanticColors.buttonText,
            fontSize: "70px",
            lineHeight: "60px",
            position: "relative" as const,
            top: "-6px",
        },
    }));

    const handleUpload = (files: FileList) => {
        if (!disabled) {
            setProgress(0);

            props.controller
                .upload(files, props.service, (percent) => setProgress(Num.floor(percent)))
                .then(() => setProgress(-1))
                .catch((err: "invalid-amount" | "invalid-extension" | "invalid-size" | string) => {
                    setError(err);
                    setProgress(-1);
                });
        }
    };

    const handleDelete = () => {
        if (props.controller.fileSlot.hasValue && !error && progress === -1) {
            setDeleting(true);

            props.controller
                .delete(props.service)
                .then(() => setDeleting(false))
                .catch(() => setDeleting(false));
        }
    };

    return (
        <FileElement
            ref={props.onAutoFocus}
            className={classes.root}
            tabIndex={props.tabIndex || 0}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onKeyDown={(e: KeyboardEvent<HTMLDivElement>) => {
                if (e.key === "Enter") {
                    if (!e.shiftKey) {
                        if (!error && progress === -1) {
                            if (props.controller.fileSlot.hasValue && props.onSubmit) {
                                e.preventDefault();

                                props.onSubmit();
                            } else if (inputRef.current) {
                                e.preventDefault();

                                inputRef.current.click();
                            }
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                } else if (e.key === "Delete" && props.controller.fileSlot.hasValue && !error && progress === -1) {
                    e.preventDefault();

                    handleDelete();
                }
            }}
            $disabled={disabled}
        >
            {!props.controller.fileSlot.hasValue && !error && progress === -1 && (
                <FileBrowseElement
                    onDragEnter={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();

                        if (!disabled && progress === -1) {
                            setDragging(true);
                        }
                    }}
                    onDragOver={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();
                    }}
                    onDragLeave={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();

                        setDragging(false);
                    }}
                    onDrop={(e: DragEvent<HTMLLabelElement>) => {
                        e.preventDefault();
                        e.stopPropagation();

                        setDragging(false);

                        if (!disabled && progress === -1) {
                            const files = e.dataTransfer.files;

                            if (files) {
                                handleUpload(files);
                            }
                        }
                    }}
                >
                    <FontIcon iconName="CloudUpload" className={classes.icon} />
                    <input
                        ref={(el) => (inputRef.current = el)}
                        type="file"
                        multiple={false}
                        tabIndex={-1}
                        disabled={disabled}
                        aria-describedby={props.ariaDescribedBy}
                        onChange={(e) => {
                            if (e.target && e.target.files) {
                                handleUpload(e.target.files);
                            }
                        }}
                    />
                    <Text block={true}>{props.labels(dragging ? "dragging" : "explanation", "")}</Text>
                    {props.controller.limit > 0 && (
                        <Text block={true} variant="xSmall">
                            {props.labels("limit", `${props.controller.limit}Mb`)}
                        </Text>
                    )}
                    {props.controller.allowedExtensions.length > 0 && (
                        <Text block={true} variant="xSmall">
                            {props.labels("extensions", Str.iterateToString(props.controller.allowedExtensions, ", "))}
                        </Text>
                    )}
                    <PrimaryButton
                        tabIndex={props.tabIndex || 0}
                        text={props.labels("browse", "")}
                        iconProps={{ iconName: "OpenFolderHorizontal" }}
                        style={{ marginTop: "8px" }}
                    />
                </FileBrowseElement>
            )}
            {!error && progress !== -1 && (
                <ProgressIndicator
                    description={props.labels("progress", `${Num.range(progress, 0, 100)}%`)}
                    percentComplete={Num.range(progress, 0, 100) / 100}
                    styles={{
                        itemProgress: {
                            width: "50%",
                        },
                    }}
                />
            )}
            {error && (
                <FileErrorElement
                    onDragOver={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    onDrop={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                >
                    <Text block={true}>{props.labels("invalid-file", "")}</Text>
                    <Text block={true}>
                        {error === "invalid-amount"
                            ? props.labels("invalid-amount", "")
                            : error === "invalid-extension"
                            ? props.labels("invalid-extension", "")
                            : error === "invalid-size"
                            ? props.labels("invalid-size", "")
                            : props.labels("error", error)}
                    </Text>
                    <PrimaryButton
                        tabIndex={props.tabIndex || 0}
                        text={props.labels("retry", "")}
                        iconProps={{ iconName: "Refresh" }}
                        onClick={() => setError("")}
                    />
                </FileErrorElement>
            )}
            {props.controller.fileSlot.hasValue && !error && progress === -1 && (
                <FilePreviewElement
                    onDragOver={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                    onDrop={(e: DragEvent<HTMLDivElement>) => {
                        e.preventDefault();
                    }}
                >
                    {props.controller.isImage ? (
                        <FileThumbnailControl
                            controller={props.controller}
                            service={props.service}
                            loading={<Spinner size={SpinnerSize.medium} />}
                            error={<FontIcon iconName="Error" className={classes.icon} />}
                        />
                    ) : (
                        <FontIcon iconName="TextDocument" className={classes.icon} />
                    )}
                    <Text block={true}>{props.controller.fileSlot.string}</Text>
                    {deleting ? (
                        <Spinner size={SpinnerSize.medium} />
                    ) : (
                        <DefaultButton
                            tabIndex={props.tabIndex || 0}
                            text={props.labels("delete", "")}
                            iconProps={{ iconName: "Delete" }}
                            onClick={handleDelete}
                        />
                    )}
                </FilePreviewElement>
            )}
        </FileElement>
    );
};
