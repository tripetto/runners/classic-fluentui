import { IProcessedStyleSet, IStyleSet, mergeStyleSets } from "@fluentui/merge-styles";
import { useCustomizationSettings } from "@fluentui/utilities";
import { ITheme } from "@fluentui/react/lib/Theme";

export function useStyles<TStyleSet extends IStyleSet>(styleFunction: (theme: ITheme) => TStyleSet): IProcessedStyleSet<TStyleSet> {
    const theme = useCustomizationSettings(["theme"]).theme;

    return mergeStyleSets(styleFunction?.(theme)) as IProcessedStyleSet<TStyleSet>;
}
