import { FocusEvent } from "react";
import { Slots, Value, findFirst, isFunction, isString } from "@tripetto/runner";
import { Dropdown } from "@fluentui/react/lib/Dropdown";

export interface IMultiSelectOption {
    readonly id: string;
    readonly name: string;
    readonly value?: Value<boolean, Slots.Boolean>;
    readonly disabled?: boolean;
}

export const MultiSelectControl = (props: {
    readonly options: IMultiSelectOption[];
    readonly id?: string;
    readonly label?: string | ((type: "field") => JSX.Element | undefined);
    readonly placeholder?: string;
    readonly required?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly ariaDescribedBy?: string;
    readonly onFocus?: (e: FocusEvent) => void;
    readonly onBlur?: (e: FocusEvent) => void;
    readonly onAutoFocus?: (el: HTMLElement | null) => void;
    readonly onSubmit?: () => void;
}) => {
    const disabled = !findFirst(props.options, (option) => !option.value?.isFrozen && !option.value?.isLocked);

    return (
        <Dropdown
            id={props.id}
            ref={props.onAutoFocus}
            multiSelect={true}
            tabIndex={props.tabIndex}
            disabled={disabled}
            label={(isString(props.label) && props.label) || undefined}
            onRenderLabel={() => (isFunction(props.label) && props.label("field")) || null}
            placeholder={props.placeholder}
            options={props.options
                .filter((option) => option.name && option.id)
                .map((option) => ({
                    key: option.id,
                    text: option.name,
                    disabled: option.disabled,
                }))}
            defaultSelectedKeys={props.options.filter((option) => option.value?.value === true).map((option) => option.id)}
            required={props.required}
            aria-describedby={props.ariaDescribedBy}
            onChange={(e, option) => {
                const changedOption = (option?.key && findFirst(props.options, (o) => o.id === option?.key)) || undefined;

                if (changedOption?.value) {
                    changedOption.value.value = option?.selected || false;
                }
            }}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            onKeyDown={(e) => {
                if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab" && !e.shiftKey && props.onSubmit) {
                    e.preventDefault();

                    props.onSubmit();
                }
            }}
        />
    );
};
