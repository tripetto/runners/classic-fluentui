import { FocusEvent, useEffect, useRef, useState } from "react";
import {
    DateTime,
    L10n,
    Num,
    Str,
    TSerializeTypes,
    cancelUITimeout,
    castToString,
    isFunction,
    isString,
    scheduleUITimeout,
} from "@tripetto/runner";
import { handleBlur, handleFocus, setReturnValue } from "./helpers";
import { TextField } from "@fluentui/react/lib/TextField";

const DEBOUNCE_MIN = 1000 / 60;
const DEBOUNCE_MAX = 1000;

export const NumberControl = (props: {
    readonly l10n?: L10n.Namespace;
    readonly id?: string;
    readonly precision?: number;
    readonly label?: string | ((type: "field") => JSX.Element | undefined);
    readonly placeholder?: string;
    readonly explanation?: string;
    readonly required?: boolean;
    readonly disabled?: boolean;
    readonly readOnly?: boolean;
    readonly error?: boolean;
    readonly tabIndex?: number;
    readonly maxLength?: number;
    readonly value?:
        | number
        | {
              pristine: TSerializeTypes;
              readonly value: number;
              readonly string: string;
              readonly hasValue: boolean;
              readonly isLocked: boolean;
              readonly isFrozen: boolean;
              readonly slot: {
                  readonly precision?: number;
                  readonly digits?: number;
                  readonly minimum?: number;
                  readonly maximum?: number;
                  readonly separator?: string;
                  readonly formatString: (value: string, plural: boolean) => string;
                  readonly toValue: (value: string) => number;
              };
          };
    readonly ariaDescribedBy?: string;
    readonly onChange?: (value: string) => string | void;
    readonly onFocus?: (e: FocusEvent) => string | void;
    readonly onBlur?: (e: FocusEvent) => string | void;
    readonly onAutoFocus?: (el: HTMLInputElement | null) => void;
    readonly onSubmit?: () => void;
    readonly onCancel?: () => void;
}) => {
    const valueRef = props.value;
    const debounceRef = useRef<{
        duration: number;
        handle: number;
        update?: () => void;
    }>({
        duration: 0,
        handle: 0,
    });
    const [type, setType] = useState<"text" | "number">("text");
    const [focus, setFocusState] = useState(false);
    const [focusValue, setFocusValue] = useState(
        typeof valueRef === "object" ? castToString(valueRef.pristine) : typeof valueRef === "number" ? valueRef.toString() : ""
    );
    const setFocus = (hasFocus: boolean) => {
        if (hasFocus) {
            setFocusValue(
                typeof valueRef === "object" ? castToString(valueRef.pristine) : typeof valueRef === "number" ? valueRef.toString() : ""
            );
        }

        setFocusState(hasFocus);
    };

    const [value, setValue] =
        typeof valueRef === "object"
            ? [
                  focus
                      ? focusValue
                      : valueRef.hasValue
                      ? valueRef.slot.formatString(
                            valueRef.slot.digits
                                ? Str.padLeft(valueRef.value, "0", valueRef.slot.digits, false, true)
                                : (props.l10n?.locale || L10n.Locales).number(
                                      valueRef.value,
                                      valueRef.slot.precision,
                                      (valueRef.slot.separator && true) || false
                                  ),
                            valueRef.value !== 1
                        )
                      : "",
                  (val: string) => {
                      cancelUITimeout(debounceRef.current.handle);

                      setFocusValue(val);

                      debounceRef.current.handle = scheduleUITimeout(
                          () => {
                              const start = DateTime.precise;

                              debounceRef.current.handle = 0;
                              debounceRef.current.update = () => {
                                  debounceRef.current.duration = DateTime.elapsed(start, true);
                                  debounceRef.current.update = undefined;
                              };
                              valueRef.pristine = val !== "" ? valueRef.slot.toValue(val) : undefined;
                              valueRef.pristine = val || undefined;
                          },
                          Num.range(debounceRef.current.duration * 2, DEBOUNCE_MIN, DEBOUNCE_MAX)
                      );
                  },
              ]
            : [focusValue, setFocusValue];
    const focusRef = useRef(0);

    useEffect(() => {
        return () => {
            cancelUITimeout(debounceRef.current.handle);
        };
    }, []);

    if (debounceRef.current.update) {
        debounceRef.current.update();
    }

    return (
        <TextField
            id={props.id}
            elementRef={props.onAutoFocus}
            type={type}
            tabIndex={props.tabIndex}
            label={(isString(props.label) && props.label) || undefined}
            onRenderLabel={() => (isFunction(props.label) && props.label("field")) || null}
            placeholder={props.placeholder}
            description={props.explanation}
            required={props.required || false}
            disabled={props.disabled || false}
            readOnly={props.readOnly || (typeof valueRef === "object" && (valueRef.isFrozen || valueRef.isLocked)) || false}
            maxLength={props.maxLength}
            value={value}
            autoComplete={"off"}
            step={
                type === "number"
                    ? (typeof valueRef === "object" && valueRef.slot.precision && `0.${Str.fill("0", valueRef.slot.precision - 1)}1`) || "1"
                    : undefined
            }
            inputMode={props.precision || (typeof valueRef === "object" && valueRef.slot.precision) ? "decimal" : "numeric"}
            aria-describedby={props.ariaDescribedBy}
            onChange={(e, v?: string) => {
                setValue(v || "");

                if (props.onChange) {
                    setReturnValue(setValue, props.onChange(v || ""));
                }
            }}
            onFocus={(e: FocusEvent<HTMLInputElement>) => {
                if (focusRef.current) {
                    cancelAnimationFrame(focusRef.current);

                    focusRef.current = 0;
                }

                if (type === "text") {
                    const el = e.target;

                    /** In Firefox we lose focus when switching input type. */
                    requestAnimationFrame(() => {
                        el.focus();
                    });

                    setType("number");
                }

                handleFocus(setFocus, setValue, props.onFocus)(e);
            }}
            onBlur={(e: FocusEvent<HTMLInputElement>) => {
                focusRef.current = requestAnimationFrame(() => {
                    focusRef.current = 0;

                    if (type === "number") {
                        setType("text");
                    }

                    if (typeof valueRef === "object" && valueRef.hasValue) {
                        setFocusValue(castToString(valueRef.pristine));
                    }

                    handleBlur(setFocus, setValue, props.onBlur)(e);
                });
            }}
            onKeyDown={(e) => {
                if (e.key === "Enter" && props.onSubmit) {
                    e.preventDefault();

                    props.onSubmit();
                } else if (e.key === "Escape") {
                    e.currentTarget.blur();
                } else if (e.key === "Tab") {
                    if (e.shiftKey) {
                        if (props.onCancel) {
                            e.preventDefault();

                            props.onCancel();
                        }
                    } else if (props.onSubmit) {
                        e.preventDefault();

                        props.onSubmit();
                    }
                }
            }}
        />
    );
};
