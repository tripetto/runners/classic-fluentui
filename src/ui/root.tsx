import { styled, StyleSheetManager, createGlobalStyle, css } from "styled-components";
import { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { Num } from "@tripetto/runner";
import { IRuntimeStyles } from "@hooks/styles";
import { Frame, FrameConfig } from "@hooks/frame";
import { Frameless } from "@hooks/frameless";
import { attach, detach } from "@helpers/resizer";
import { MARGIN, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "@ui/const";

const RootFrame = styled(Frame).withConfig(FrameConfig)<{
    $view: TRunnerViews;
    $isPage: boolean;
}>`
    background-color: transparent !important;
    border: none !important;
    position: ${(props) => (props.$view === "live" && props.$isPage ? "fixed" : undefined)};
    left: ${(props) => (props.$view === "live" && props.$isPage ? 0 : undefined)};
    top: ${(props) => (props.$view === "live" && props.$isPage ? 0 : undefined)};
    width: 100%;
    height: ${(props) => (props.$view === "live" && !props.$isPage ? 0 : "100%")};
`;

const RootBody = createGlobalStyle<{
    $customCSS?: string;
}>`
    body {
        overflow: hidden;
        background-color: transparent;

        ${(props) =>
            props.$customCSS &&
            css`
                ${props.$customCSS}
            `}
    }
`;

const RootElement = styled.div<{
    $view: TRunnerViews;
    $isPage: boolean;
    $styles: IRuntimeStyles;
}>`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    font-variant-ligatures: none;
    background-color: ${(props) => props.$styles.backgroundColor};
    background-position: center center;
    overflow-x: ${(props) => (props.$view === "live" && !props.$isPage ? "visible" : "hidden")};
    overflow-y: ${(props) => (props.$view === "live" && !props.$isPage ? "visible" : "auto")};
    scroll-behavior: smooth;
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    > div {
        max-width: ${(props) => props.$isPage && "1140px"};
        margin-left: ${(props) => props.$isPage && "auto"};
        margin-right: ${(props) => props.$isPage && "auto"};
        padding: ${(props) => (props.$isPage || props.$view !== "live" ? `${MARGIN}px` : undefined)};
    }

    * {
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    a {
        text-decoration: underline;
        color: inherit !important;
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        > div {
            padding: ${(props) => (props.$isPage || props.$view !== "live" ? `${SMALL_SCREEN_MARGIN}px` : undefined)};
        }
    }
`;

const Content = (props: { readonly children?: ReactNode; readonly onChange: (height: number) => void }) => {
    const contentRef = useRef<HTMLElement>() as MutableRefObject<HTMLDivElement>;

    useEffect(() => {
        const ref = attach(() => {
            if (contentRef.current) {
                props.onChange(contentRef.current.getBoundingClientRect().height);
            }
        });

        return () => detach(ref);
    });

    return <div ref={contentRef}>{props.children}</div>;
};

export const ClassicRoot = (props: {
    readonly frameRef?: MutableRefObject<HTMLIFrameElement>;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly title?: string;
    readonly styles: IRuntimeStyles;
    readonly className?: string;
    readonly customStyle?: CSSProperties;
    readonly customCSS?: string;
    readonly children?: ReactNode;
    readonly onTouch?: () => void;
}) => {
    const [contentHeight, setContentHeight] = useState(0);

    useEffect(() => {
        if (props.frameRef && props.frameRef.current && props.view === "live" && (props.isPage || props.styles.autoFocus)) {
            props.frameRef.current.focus();
        }
    }, [contentHeight]);

    return props.frameRef ? (
        <RootFrame
            frameRef={props.frameRef}
            title={props.title}
            style={{
                ...props.customStyle,
                height:
                    props.customStyle && props.customStyle.height && props.customStyle.height !== "auto"
                        ? props.customStyle.height
                        : (props.view === "live" && !props.isPage && contentHeight) || undefined,
            }}
            className={props.className}
            onTouch={props.onTouch}
            $view={props.view}
            $isPage={props.isPage}
        >
            {(doc: Document) => (
                <StyleSheetManager target={doc.head}>
                    <>
                        <RootBody $customCSS={props.customCSS} />
                        <RootElement $view={props.view} $isPage={props.isPage} $styles={props.styles}>
                            <Content
                                onChange={(height: number) => {
                                    height = Num.ceil(height);

                                    if (height !== contentHeight) {
                                        setContentHeight(height);
                                    }
                                }}
                            >
                                {props.children}
                            </Content>
                        </RootElement>
                    </>
                </StyleSheetManager>
            )}
        </RootFrame>
    ) : (
        <Frameless
            theme={props.styles.theme}
            view={props.view}
            isPage={props.isPage}
            styles={props.styles}
            className={props.className}
            onTouch={props.onTouch}
            style={{
                ...props.customStyle,
                height:
                    props.customStyle && props.customStyle.height && props.customStyle.height !== "auto"
                        ? props.customStyle.height
                        : undefined,
            }}
        >
            {props.children}
        </Frameless>
    );
};
