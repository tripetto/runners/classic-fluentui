import { L10n } from "@tripetto/runner";
import { Stack } from "@fluentui/react/lib/Stack";
import { MessageBar, MessageBarType } from "@fluentui/react/lib/MessageBar";

export const ConnectionError = (props: { readonly l10n: L10n.Namespace }) => (
    <Stack style={{ marginTop: "24px" }}>
        <MessageBar delayedRender={false} messageBarType={MessageBarType.error}>
            <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "Something went wrong while submitting your conversation.")}</b>
            <br />
            {props.l10n.pgettext(
                "runner#9|⚠ Errors|Connection error",
                "Please check your connection and try again (for the techies: The error console of your browser might contain more technical information about what went wrong)."
            )}
        </MessageBar>
    </Stack>
);
