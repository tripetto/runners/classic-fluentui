import { L10n } from "@tripetto/runner";
import { Stack } from "@fluentui/react/lib/Stack";
import { MessageBar, MessageBarType } from "@fluentui/react/lib/MessageBar";

export const RejectedError = (props: { readonly l10n: L10n.Namespace }) => (
    <Stack style={{ marginTop: "24px" }}>
        <MessageBar delayedRender={false} messageBarType={MessageBarType.error}>
            <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "Your data is rejected.")}</b>
            <br />
            {props.l10n.pgettext(
                "runner#9|⚠ Errors|Rejected error",
                "Unfortunately, your data is marked as invalid and therefore rejected. If you believe this is a mistake, please contact the form owner. We're sorry for the inconvenience."
            )}
        </MessageBar>
    </Stack>
);
