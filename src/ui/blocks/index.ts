import { styled, css } from "styled-components";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { MARGIN, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE, SPACING } from "@ui/const";

export const Blocks = styled.div<{
    $view: TRunnerViews;
    $center?: boolean;
    $margin?: number;
}>`
    display: ${(props) => (props.$center ? "flex" : "block")};
    min-height: ${(props) => props.$center && `calc(100% - ${MARGIN * 2}px)`};
    justify-content: ${(props) => props.$center && "center"};
    align-items: ${(props) => props.$center && "center"};
    margin-top: ${(props) => props.$margin && `${props.$margin}px`};
    flex-direction: column;

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        min-height: ${(props) => props.$center && `calc(100% - ${SMALL_SCREEN_MARGIN * 2}px)`};
    }

    ${(props) =>
        props.$center &&
        css`
            > div {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 100%;
            }
        `}
`;

export const Block = styled.div<{
    $locked?: boolean;
}>`
    position: relative;
    margin-top: ${SPACING}px;
    opacity: ${(props) => (props.$locked && 0.2) || undefined};
    pointer-events: ${(props) => (props.$locked && "none") || undefined};
    max-width: 100%;

    &:first-child {
        margin-top: 0;
    }

    &:last-child {
        margin-bottom: 0;
    }

    > * {
        &:first-child {
            margin-top: 0 !important;
        }
    }

    > label + span {
        margin-top: 0 !important;
        margin-bottom: 0.5714285714285714em !important;
    }
`;
