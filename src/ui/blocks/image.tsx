import { styled } from "styled-components";
import { onLoad } from "@helpers/resizer";
import { Image } from "@fluentui/react/lib/Image";

const ImageContainer = styled.div<{
    $aligment: "left" | "center" | "right";
    $margin: boolean;
}>`
    width: 100%;
    display: flex;
    justify-content: ${(props) => props.$aligment};
    margin-bottom: ${(props) => props.$margin && "16px"};
`;

export const BlockImage = (props: {
    readonly src: string;
    readonly isPage: boolean;
    readonly width?: string;
    readonly alignment?: "left" | "center" | "right";
    readonly margin?: boolean;
    readonly onClick?: () => void;
}) => (
    <ImageContainer onClick={props.onClick} $aligment={props.alignment || "left"} $margin={props.margin || false}>
        <Image
            src={props.src}
            width={props.width}
            onLoad={onLoad}
            style={{
                maxHeight: props.isPage ? "50vh" : undefined,
            }}
        />
    </ImageContainer>
);
