import { useState } from "react";
import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { verifyEmail } from "@helpers/verify";
import { IPausingRecipeEmail } from "@interfaces/pausing";
import { Block, Blocks } from ".";
import { Text } from "@fluentui/react/lib/Text";
import { Stack } from "@fluentui/react/lib/Stack";
import { DefaultButton, PrimaryButton } from "@fluentui/react/lib/Button";
import { InputControl } from "@ui/controls/input";

const PauseForm = (props: { readonly controller: IPausingRecipeEmail; readonly l10n: L10n.Namespace; readonly view: TRunnerViews }) => {
    const [value, setValue] = useState(props.controller?.emailAddress || "");
    const [hadFocus, setFocus] = useState(false);
    const isValid = verifyEmail(value);

    return (
        <Blocks $view={props.view} $margin={16}>
            <Block>
                <Text block={true} variant="xxLarge" style={{ marginBottom: "8px" }}>
                    {props.l10n.pgettext("runner#8|⏸ Pause conversation", "Pause this conversation")}
                </Text>
                <Text block={true} style={{ marginBottom: "16px" }}>
                    {props.l10n.pgettext(
                        "runner#8|⏸ Pause conversation",
                        "Receive a link by email to resume later on any device, right where you left off."
                    )}
                </Text>
                <InputControl
                    type="email"
                    inputMode="email"
                    autoComplete="email"
                    required={true}
                    value={value}
                    placeholder={props.l10n.pgettext("runner#8|⏸ Pause conversation", "Your email address...")}
                    error={hadFocus && !isValid}
                    disabled={props.controller.isCompleting}
                    onChange={setValue}
                    onFocus={() => setFocus(true)}
                    onAutoFocus={(el) => {
                        if (el) {
                            el.focus();
                        }
                    }}
                    onSubmit={() => isValid && props.controller.complete(value)}
                />
                <Stack horizontal tokens={{ childrenGap: 12 }} style={{ marginTop: "24px" }}>
                    <PrimaryButton
                        iconProps={{ iconName: "Accept" }}
                        text={
                            props.controller && props.controller.isCompleting
                                ? props.l10n.pgettext("runner#3|🩺 Status information", "Pausing...")
                                : props.l10n.pgettext("runner#8|⏸ Pause conversation", "Receive resume link")
                        }
                        disabled={props.controller.isCompleting || !isValid}
                        onClick={() => props.controller.complete(value)}
                    />
                    <DefaultButton
                        iconProps={{ iconName: "Cancel" }}
                        text={props.l10n.pgettext("runner#8|⏸ Pause conversation", "Cancel pausing")}
                        disabled={props.controller.isCompleting}
                        onClick={() => props.controller.cancel()}
                    />
                </Stack>
            </Block>
        </Blocks>
    );
};

export const PauseBlock = (props: {
    readonly controller: IPausingRecipeEmail | undefined;
    readonly l10n: L10n.Namespace;
    readonly view: TRunnerViews;
}) => (props.controller && <PauseForm controller={props.controller} l10n={props.l10n} view={props.view} />) || <></>;
