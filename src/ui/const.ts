/** Margin around the content in full page mode. */
export const MARGIN = 32;

/** Spacing between blocks. */
export const SPACING = 8;

/** Number of pixels to indicate a small screen. */
export const SMALL_SCREEN_SIZE = 500;

/** Margin around the content in small screen mode. */
export const SMALL_SCREEN_MARGIN = 16;
