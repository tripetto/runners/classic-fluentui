import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { Block, Blocks } from "@ui/blocks";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";

export const PausedMessage = (props: { readonly l10n: L10n.Namespace; readonly view: TRunnerViews; readonly isPage: boolean }) => (
    <Blocks $view={props.view} $center={props.isPage}>
        <Block>
            <Text
                block={true}
                variant="xxLarge"
                style={{
                    textAlign: props.isPage ? "center" : "left",
                    marginBottom: "8px",
                }}
            >
                <FontIcon iconName="CirclePause" style={{ fontSize: 25, marginRight: "12px", position: "relative", top: "3px" }} />
                {props.l10n.pgettext("runner#2|💬 Messages|Form paused", "Form paused")}
            </Text>
            <Text
                block={true}
                style={{
                    textAlign: props.isPage ? "center" : "left",
                }}
            >
                {props.l10n.pgettext("runner#2|💬 Messages|Form paused", "The form is now paused.")}
            </Text>
        </Block>
    </Blocks>
);
