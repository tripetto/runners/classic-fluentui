import { L10n } from "@tripetto/runner";
import { Stack } from "@fluentui/react/lib/Stack";
import { Spinner, SpinnerSize } from "@fluentui/react/lib/Spinner";
import { Text } from "@fluentui/react/lib/Text";

export const EvaluatingMessage = (props: { readonly l10n: L10n.Namespace }) => (
    <Stack
        horizontal
        tokens={{ childrenGap: 8 }}
        style={{
            marginTop: "24px",
        }}
    >
        <Spinner size={SpinnerSize.medium} />
        <Text>{props.l10n.pgettext("runner#3|🩺 Status information", "One moment please...")}</Text>
    </Stack>
);
