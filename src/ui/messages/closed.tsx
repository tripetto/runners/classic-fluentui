import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { Block, Blocks } from "@ui/blocks";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";

export const ClosedMessage = (props: { readonly l10n: L10n.Namespace; readonly view: TRunnerViews; readonly isPage: boolean }) => (
    <Blocks $view={props.view} $center={props.isPage}>
        <Block>
            <Text
                block={true}
                style={{
                    textAlign: props.isPage ? "center" : "left",
                    marginBottom: "8px",
                }}
                variant="xxLarge"
            >
                <FontIcon iconName="ErrorBadge" style={{ fontSize: 25, marginRight: "12px", position: "relative", top: "3px" }} />
                {props.l10n.pgettext("runner#2|💬 Messages|Form closed", "Form closed")}
            </Text>
            <Text
                block={true}
                style={{
                    textAlign: props.isPage ? "center" : "left",
                }}
            >
                {props.l10n.pgettext("runner#2|💬 Messages|Form closed", "This form is currently not available.")}
            </Text>
        </Block>
    </Blocks>
);
