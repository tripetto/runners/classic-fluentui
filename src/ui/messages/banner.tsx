import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";

const BannerElement = styled.div`
    display: block;
    margin-top: 12px;

    a {
        text-decoration: none;
        color: inherit !important;
        transition: opacity 0.5s;
        opacity: 0.4;

        > i {
            position: relative;
            top: 1px;
            margin-right: 4px;
            font-size: 11px;
        }

        &:hover {
            opacity: 1;
            text-decoration: none;
        }
    }
`;

export const Banner = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) =>
    !props.styles.noBranding ? (
        <BannerElement>
            <Text variant="small">
                <a
                    href="https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_classic_fluentui&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=form"
                    target="_blank"
                >
                    <FontIcon iconName="Heart" />
                    {props.l10n.pgettext("runner:classic:fluentui", "Powered by Tripetto")}
                </a>
            </Text>
        </BannerElement>
    ) : (
        <></>
    );
