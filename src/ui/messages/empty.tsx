import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { Block, Blocks } from "@ui/blocks";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";

export const EmptyMessage = (props: { readonly l10n: L10n.Namespace; readonly view: TRunnerViews }) => (
    <Blocks $view={props.view} $center={true}>
        <Block>
            <Text
                block={true}
                style={{
                    textAlign: "center",
                    marginBottom: "8px",
                }}
                variant="xxLarge"
            >
                <FontIcon iconName="PageHeaderEdit" style={{ fontSize: 25, marginRight: "12px", position: "relative", top: "3px" }} />
                {props.l10n.pgettext("runner:classic:fluentui", "Form is empty")}
            </Text>
            <Text
                block={true}
                style={{
                    textAlign: "center",
                }}
            >
                {props.l10n.pgettext("runner:classic:fluentui", "Please add a block first to get the magic going.")}
            </Text>
        </Block>
    </Blocks>
);
