import { IEpilogue, L10n, isBoolean, markdownifyToString, markdownifyToURL } from "@tripetto/runner";
import { TRunnerViews, markdownifyToJSX } from "@tripetto/runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { Block, Blocks } from "@ui/blocks";
import { BlockImage } from "@ui/blocks/image";
import { BlockVideo } from "@ui/blocks/video";
import { LinkElement } from "@ui/controls/link";
import { Text } from "@fluentui/react/lib/Text";
import { Stack } from "@fluentui/react/lib/Stack";
import { PrimaryButton } from "@fluentui/react/lib/Button";
import { FontIcon } from "@fluentui/react/lib/Icon";

export const Epilogue = (
    props: IEpilogue & {
        readonly l10n: L10n.Namespace;
        readonly styles: IRuntimeStyles;
        readonly view: TRunnerViews;
        readonly isPage: boolean;
        readonly repeat?: () => void;
        readonly edit?: () => void;
    }
) => {
    if (props.view === "live" && props.getRedirect && props.getRedirect()) {
        return <></>;
    }

    return (
        <Blocks $view={props.view} $center={props.isPage}>
            <Block>
                {props.view !== "live" && props.redirectUrl ? (
                    <>
                        <Text
                            block={true}
                            variant="xxLarge"
                            onClick={props.edit}
                            style={{
                                textAlign: props.isPage ? "center" : "left",
                                marginBottom: "8px",
                            }}
                        >
                            <FontIcon
                                iconName={props.view === "preview" ? "OpenInNewWindow" : "Completed"}
                                style={{ fontSize: 25, marginRight: "12px", position: "relative", top: "3px" }}
                            />
                            {props.view === "preview"
                                ? props.l10n.pgettext("runner:classic:fluentui", "Redirect preview")
                                : props.l10n.pgettext("runner:classic:fluentui", "Test completed")}
                        </Text>
                        <Text
                            block={true}
                            onClick={props.edit}
                            style={{
                                textAlign: props.isPage ? "center" : "left",
                            }}
                        >
                            {props.l10n.pgettext("runner:classic:fluentui", "In a live environment the form will redirect to:")}
                            <br />
                            <LinkElement href={markdownifyToURL(props.redirectUrl, props.context) || "#"} target="_blank">
                                {markdownifyToURL(props.redirectUrl, props.context) ||
                                    props.l10n.pgettext("runner:classic:fluentui", "The supplied URL is invalid!")}
                            </LinkElement>
                        </Text>
                        {props.view === "test" && (
                            <Stack
                                style={{
                                    marginTop: "16px",
                                    alignItems: props.isPage ? "center" : "start",
                                }}
                            >
                                <PrimaryButton
                                    iconProps={{ iconName: "Refresh" }}
                                    text={props.l10n.pgettext("runner:classic:fluentui", "Test again")}
                                    disabled={!props.repeat}
                                    onClick={props.repeat}
                                />
                            </Stack>
                        )}
                    </>
                ) : (
                    <>
                        {props.image && (
                            <BlockImage
                                src={markdownifyToURL(props.image, props.context, undefined, [
                                    "image/jpeg",
                                    "image/png",
                                    "image/svg",
                                    "image/gif",
                                ])}
                                alignment="center"
                                margin={true}
                                isPage={props.isPage}
                                onClick={props.edit}
                            />
                        )}
                        <Text
                            block={true}
                            variant="xxLarge"
                            onClick={props.edit}
                            style={{
                                textAlign: props.isPage ? "center" : "left",
                                marginBottom: "8px",
                            }}
                        >
                            <FontIcon
                                iconName="Completed"
                                style={{ fontSize: 25, marginRight: "12px", position: "relative", top: "3px" }}
                            />
                            {markdownifyToJSX(
                                props.title ||
                                    (props.view === "test" && props.l10n.pgettext("runner:classic:fluentui", "Test completed")) ||
                                    props.l10n.pgettext("runner#2|💬 Messages|Form completed", "Form completed"),
                                props.context
                            )}
                        </Text>
                        {props.description ? (
                            <Text
                                block={true}
                                onClick={props.edit}
                                style={{
                                    textAlign: props.isPage ? "center" : "left",
                                }}
                            >
                                {markdownifyToJSX(props.description, props.context)}
                            </Text>
                        ) : (
                            !props.title &&
                            props.view === "test" && (
                                <Text
                                    block={true}
                                    onClick={props.edit}
                                    style={{
                                        textAlign: props.isPage ? "center" : "left",
                                    }}
                                >
                                    {props.l10n.pgettext("runner:classic:fluentui", "Did you know you can customize this closing message?")}
                                </Text>
                            )
                        )}
                        {props.video && (
                            <BlockVideo src={markdownifyToURL(props.video, props.context)} view={props.view} play={true} margin={true} />
                        )}
                        {(props.button || props.repeatable || props.view === "test") && (
                            <Stack
                                horizontal
                                tokens={{ childrenGap: 8 }}
                                style={{
                                    marginTop: "16px",
                                    alignItems: props.isPage ? "center" : "start",
                                }}
                            >
                                {props.button && (
                                    <PrimaryButton
                                        text={markdownifyToString(props.button.label, props.context)}
                                        onClick={(e) => {
                                            e.stopPropagation();

                                            window.open(
                                                markdownifyToURL(props.button!.url, props.context),
                                                props.view === "preview" || props.view === "test" || props.button!.target === "blank"
                                                    ? "_blank"
                                                    : "_self",
                                                "noopener"
                                            );
                                        }}
                                    />
                                )}
                                {(props.repeatable || props.view === "test") && (
                                    <>
                                        <PrimaryButton
                                            iconProps={{ iconName: "Refresh" }}
                                            text={
                                                props.repeatable
                                                    ? props.l10n.pgettext("runner#1|🆗 Buttons", "Start again")
                                                    : props.l10n.pgettext("runner:classic:fluentui", "Test again")
                                            }
                                            disabled={!props.repeat}
                                            onClick={props.repeat}
                                        />
                                        {props.edit &&
                                            !props.image &&
                                            !props.title &&
                                            !props.description &&
                                            !props.video &&
                                            !props.button &&
                                            !isBoolean(props.repeatable) && (
                                                <PrimaryButton
                                                    iconProps={{ iconName: "Edit" }}
                                                    text={props.l10n.pgettext("runner:classic:fluentui", "Customize")}
                                                    onClick={props.edit}
                                                />
                                            )}
                                    </>
                                )}
                            </Stack>
                        )}
                    </>
                )}
            </Block>
            {props.view === "live" && !props.styles.noBranding && (
                <Block>
                    <Text
                        block={true}
                        variant="xLarge"
                        style={{
                            textAlign: props.isPage ? "center" : "left",
                            marginTop: "16px",
                        }}
                    >
                        {props.l10n.pgettext("runner:classic:fluentui", "Want to make a form like this for free?")}
                    </Text>
                    <Text
                        block={true}
                        onClick={props.edit}
                        style={{
                            textAlign: props.isPage ? "center" : "left",
                        }}
                    >
                        {props.l10n.pgettext(
                            "runner:classic:fluentui",
                            "Tripetto is for making elegantly personal form and survey experiences with response boosting conversational powers."
                        )}
                    </Text>
                    <Stack
                        style={{
                            marginTop: "16px",
                            alignItems: props.isPage ? "center" : "start",
                        }}
                    >
                        <PrimaryButton
                            iconProps={{ iconName: "PageHeaderEdit" }}
                            text={props.l10n.pgettext("runner:classic:fluentui", "Create one now!")}
                            onClick={(e) => {
                                e.stopPropagation();

                                window.open(
                                    "https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_classic&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=closing",
                                    "_blank",
                                    "noopener"
                                );
                            }}
                        />
                    </Stack>
                </Block>
            )}
        </Blocks>
    );
};
