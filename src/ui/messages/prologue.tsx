import { IPrologue, L10n } from "@tripetto/runner";
import { TRunnerViews, markdownifyToJSX } from "@tripetto/runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { Block, Blocks } from "@ui/blocks";
import { BlockImage } from "@ui/blocks/image";
import { BlockVideo } from "@ui/blocks/video";
import { Banner } from "@ui/messages/banner";
import { Text } from "@fluentui/react/lib/Text";
import { Stack } from "@fluentui/react/lib/Stack";
import { PrimaryButton } from "@fluentui/react/lib/Button";

export const Prologue = (
    props: IPrologue & {
        readonly l10n: L10n.Namespace;
        readonly styles: IRuntimeStyles;
        readonly view: TRunnerViews;
        readonly isPage: boolean;
        readonly start: () => void;
        readonly edit?: () => void;
    }
) => (
    <Blocks $view={props.view} $center={props.isPage}>
        <Block>
            {props.image && (
                <BlockImage
                    src={props.image}
                    alignment={props.isPage ? "center" : "left"}
                    margin={props.title || props.description || props.video ? true : false}
                    isPage={props.isPage}
                    onClick={props.edit}
                />
            )}
            {props.title && (
                <Text
                    block={true}
                    variant="xxLarge"
                    onClick={props.edit}
                    style={{
                        textAlign: props.isPage ? "center" : "left",
                        marginBottom: "8px",
                    }}
                >
                    {markdownifyToJSX(props.title)}
                </Text>
            )}
            {props.description && (
                <Text
                    block={true}
                    onClick={props.edit}
                    style={{
                        textAlign: props.isPage ? "center" : "left",
                    }}
                >
                    {markdownifyToJSX(props.description)}
                </Text>
            )}
            {props.video && <BlockVideo src={props.video} view={props.view} margin={props.title || props.description ? true : false} />}
            <Stack
                style={{
                    marginTop: "16px",
                    alignItems: props.isPage ? "center" : "start",
                }}
            >
                <PrimaryButton
                    iconProps={{ iconName: "ChevronRight" }}
                    text={props.button || props.l10n.pgettext("runner#1|🆗 Buttons", "Start")}
                    onClick={props.start}
                    elementRef={(e) => {
                        if (props.view === "live" && (props.styles.autoFocus || props.isPage) && e) {
                            e.focus();
                        }
                    }}
                />
            </Stack>
            {props.view !== "preview" && <Banner l10n={props.l10n} styles={props.styles} />}
        </Block>
    </Blocks>
);
