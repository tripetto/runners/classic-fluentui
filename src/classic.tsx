import { StrictMode } from "react";
import { useClassicRunner } from "@hooks/runner";
import { IClassicUIProps } from "@interfaces/props";
import { ClassicRoot } from "@ui/root";
import { EvaluatingMessage } from "@ui/messages/evaluating";
import { EmptyMessage } from "@ui/messages/empty";
import { ClosedMessage } from "@ui/messages/closed";
import { PausedMessage } from "@ui/messages/paused";
import { Prologue } from "@ui/messages/prologue";
import { Epilogue } from "@ui/messages/epilogue";
import { Banner } from "@ui/messages/banner";
import { ConnectionError } from "@ui/errors/connection";
import { OutdatedError } from "@ui/errors/outdated";
import { RejectedError } from "@ui/errors/rejected";
import { PauseError } from "@ui/errors/pause";
import { PauseBlock } from "@ui/blocks/pause";
import { DefaultButton, IconButton, PrimaryButton } from "@fluentui/react/lib/Button";
import { Stack } from "@fluentui/react/lib/Stack";
import { ProgressIndicator } from "@fluentui/react/lib/ProgressIndicator";
import { initializeIcons } from "@fluentui/react/lib/Icons";
import "@blocks";

initializeIcons(undefined, { disableWarnings: true });

export const ClassicUI = (props: IClassicUIProps) => {
    const {
        status,
        view,
        mode,
        frameRef,
        title,
        l10n,
        styles,
        prologue,
        blocks,
        buttons,
        pausing,
        progressPercentage,
        pages,
        epilogue,
        isEvaluating,
        isPage,
        tabIndex,
    } = useClassicRunner(props);

    return (
        <StrictMode>
            <ClassicRoot
                frameRef={props.useFrame ? frameRef : undefined}
                view={view}
                isPage={isPage}
                title={title}
                styles={styles}
                className={props.className}
                customStyle={props.customStyle}
                customCSS={props.customCSS}
                onTouch={props.onTouch}
            >
                {(prologue && <Prologue {...prologue} />) ||
                    (blocks && (
                        <>
                            {blocks}
                            {isEvaluating && <EvaluatingMessage l10n={l10n} />}
                            {status === "error" && <ConnectionError l10n={l10n} />}
                            {status === "error-outdated" && <OutdatedError l10n={l10n} allowReload={props.onReload ? true : false} />}
                            {status === "error-rejected" && <RejectedError l10n={l10n} />}
                            {status === "error-paused" && <PauseError l10n={l10n} />}
                            {buttons && (
                                <Stack horizontal tokens={{ childrenGap: 12 }} style={{ marginTop: "24px" }}>
                                    {mode === "progressive" || mode === "ahead" ? (
                                        <PrimaryButton
                                            tabIndex={tabIndex}
                                            disabled={
                                                status === "finishing" ||
                                                status === "reloading" ||
                                                !buttons.finishable ||
                                                !(buttons.reload || buttons.finish)
                                            }
                                            iconProps={{
                                                iconName:
                                                    status === "finishing"
                                                        ? "HourGlass"
                                                        : status === "error" ||
                                                          (status === "error-outdated" && props.onReload) ||
                                                          status === "reloading"
                                                        ? "Refresh"
                                                        : "CheckMark",
                                            }}
                                            text={
                                                status === "finishing"
                                                    ? l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                                                    : (status === "error-outdated" && props.onReload) || status === "reloading"
                                                    ? l10n.pgettext("runner#1|🆗 Buttons", "Reload")
                                                    : status === "error"
                                                    ? l10n.pgettext("runner#1|🆗 Buttons", "Retry")
                                                    : l10n.pgettext("runner#1|🆗 Buttons", "Submit")
                                            }
                                            onClick={buttons.reload || buttons.finish}
                                        />
                                    ) : (
                                        <>
                                            <PrimaryButton
                                                tabIndex={tabIndex}
                                                iconProps={{
                                                    iconName:
                                                        status === "finishing"
                                                            ? "HourGlass"
                                                            : status === "error" ||
                                                              (status === "error-outdated" && props.onReload) ||
                                                              status === "reloading"
                                                            ? "Refresh"
                                                            : buttons.finish
                                                            ? "CheckMark"
                                                            : "ChevronRight",
                                                }}
                                                disabled={
                                                    status === "finishing" ||
                                                    status === "reloading" ||
                                                    (buttons.finish && !buttons.finishable) ||
                                                    !(buttons.reload || buttons.next)
                                                }
                                                text={
                                                    status === "finishing"
                                                        ? l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                                                        : buttons.finish
                                                        ? (status === "error-outdated" && props.onReload) || status === "reloading"
                                                            ? l10n.pgettext("runner#1|🆗 Buttons", "Reload")
                                                            : status === "error"
                                                            ? l10n.pgettext("runner#1|🆗 Buttons", "Retry")
                                                            : l10n.pgettext("runner#1|🆗 Buttons", "Submit")
                                                        : l10n.pgettext("runner#1|🆗 Buttons", "Next")
                                                }
                                                onClick={buttons.reload || buttons.next}
                                            />
                                            {buttons.previous && (
                                                <DefaultButton
                                                    iconProps={{ iconName: "ChevronLeft" }}
                                                    text={l10n.pgettext("runner#1|🆗 Buttons", "Back")}
                                                    tabIndex={tabIndex}
                                                    disabled={status === "finishing"}
                                                    onClick={buttons.previous}
                                                />
                                            )}
                                        </>
                                    )}
                                    {buttons.pause && (
                                        <IconButton
                                            iconProps={{ iconName: "Pause" }}
                                            title={l10n.pgettext("runner#1|🆗 Buttons", "Pause")}
                                            tabIndex={tabIndex}
                                            disabled={status === "finishing"}
                                            onClick={buttons.pause}
                                        />
                                    )}
                                    <Stack.Item grow>
                                        {mode === "paginated" && styles.showProgressbar && (
                                            <Stack
                                                verticalAlign="center"
                                                verticalFill
                                                style={{ position: "relative", top: "-2px", marginBottom: "-2px" }}
                                            >
                                                <ProgressIndicator
                                                    percentComplete={progressPercentage / 100}
                                                    description={`${progressPercentage}%`}
                                                />
                                            </Stack>
                                        )}
                                    </Stack.Item>
                                    {mode === "paginated" && styles.showPageIndicators && pages.length > 0 && (
                                        <Stack horizontal wrap tokens={{ childrenGap: 4 }}>
                                            {pages.map((page, index) =>
                                                page.active ? (
                                                    <PrimaryButton
                                                        key={index}
                                                        text={`${page.number}`}
                                                        disabled={status === "finishing"}
                                                        onClick={page.activate}
                                                        style={{ minWidth: "auto" }}
                                                    />
                                                ) : (
                                                    <DefaultButton
                                                        key={index}
                                                        text={`${page.number}`}
                                                        disabled={status === "finishing"}
                                                        onClick={page.activate}
                                                        style={{ minWidth: "auto" }}
                                                    />
                                                )
                                            )}
                                        </Stack>
                                    )}
                                </Stack>
                            )}
                            <PauseBlock controller={pausing} l10n={l10n} view={view} />
                            {(view === "live" || (view === "test" && status !== "empty")) && <Banner l10n={l10n} styles={styles} />}
                        </>
                    )) ||
                    (epilogue && <Epilogue {...epilogue} />) ||
                    (view !== "live" && (status === "empty" || status === "preview") && <EmptyMessage l10n={l10n} view={view} />) ||
                    (status === "paused" && <PausedMessage l10n={l10n} view={view} isPage={isPage} />) || (
                        <ClosedMessage l10n={l10n} view={view} isPage={isPage} />
                    )}
            </ClassicRoot>
        </StrictMode>
    );
};
