import "./polyfills";

import "@namespace/mount";
export { ClassicUI } from "./classic";
export { IClassic } from "@interfaces/classic";
export { IClassicProps, TClassicPause } from "@interfaces/props";
export { IClassicSnapshot } from "@interfaces/snapshot";
export { IClassicRunner } from "@interfaces/runner";
export { IClassicStyles } from "@interfaces/styles";
export { IClassicController } from "@hooks/controller";
export { IClassicRendering, IClassicRenderProps } from "@interfaces/block";
export { IBuilderInstance } from "@interfaces/builder";
export { run, ClassicRunner } from "./run";
export { namespace } from "@namespace";
import "@namespace/unmount";

/** Export local styled-components for creating custom blocks. */
import { styled } from "styled-components";
export { styled };
export { css, keyframes } from "styled-components";
