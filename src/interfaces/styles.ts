import { TStyles } from "@tripetto/runner";

export interface IClassicStyles extends TStyles {
    readonly mode?: "paginated" | "continuous" | "progressive" | "ahead";
    readonly autoFocus?: boolean;
    readonly noBranding?: boolean;
    readonly showEnumerators?: boolean;
    readonly showPageIndicators?: boolean;
    readonly showProgressbar?: boolean;
    readonly color?: string;
    readonly textColor?: string;
    readonly backgroundColor?: string;
    readonly palette?: {
        readonly themePrimary?: string;
        readonly themeLighterAlt?: string;
        readonly themeLighter?: string;
        readonly themeLight?: string;
        readonly themeTertiary?: string;
        readonly themeSecondary?: string;
        readonly themeDarkAlt?: string;
        readonly themeDark?: string;
        readonly themeDarker?: string;
        readonly neutralLighterAlt?: string;
        readonly neutralLighter?: string;
        readonly neutralLight?: string;
        readonly neutralQuaternaryAlt?: string;
        readonly neutralQuaternary?: string;
        readonly neutralTertiaryAlt?: string;
        readonly neutralTertiary?: string;
        readonly neutralSecondary?: string;
        readonly neutralSecondaryAlt?: string;
        readonly neutralPrimaryAlt?: string;
        readonly neutralPrimary?: string;
        readonly neutralDark?: string;
        readonly black?: string;
        readonly white?: string;
    };
}
