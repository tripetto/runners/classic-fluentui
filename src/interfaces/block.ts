import { L10n, NodeBlock } from "@tripetto/runner";
import { FocusEvent, ReactNode } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { IRunnerAttachments, TRunnerViews } from "@tripetto/runner-react-hook";

export interface IClassicRenderProps {
    readonly id: string;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;

    readonly name: string;

    /** Block title. */
    readonly title: JSX.Element | undefined;

    /** Label for the block (in case there is also a description present). */
    readonly label: JSX.Element | undefined;

    /** Renderer for the field label. */
    readonly fieldLabel: (type: "field" | "checkbox") => JSX.Element | undefined;

    /** Description for the block. */
    readonly description: JSX.Element | undefined;

    /** Additional explanation for the block. */
    readonly explanation: string | undefined;

    /** Placeholder for the block field. */
    readonly placeholder: string;

    readonly tabIndex: number;
    readonly isFailed: boolean;
    readonly isPage: boolean;
    readonly ariaDescribedBy: string | undefined;
    readonly ariaDescription: JSX.Element | undefined;
    readonly focus: <T>(e: FocusEvent) => T;
    readonly blur: <T>(e: FocusEvent) => T;
    readonly autoFocus: (element: HTMLElement | null) => void;
    readonly onSubmit: (() => void) | undefined;
    readonly attachments: IRunnerAttachments | undefined;
    readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => JSX.Element;
    readonly markdownifyToURL: (md: string) => string;
    readonly markdownifyToImage: (md: string) => string;
    readonly markdownifyToString: (md: string) => string;
}

export interface IClassicRendering extends NodeBlock {
    readonly required?: boolean;
    readonly useLabelAsTitle?: boolean;
    readonly render?: (props: IClassicRenderProps) => ReactNode;
}
