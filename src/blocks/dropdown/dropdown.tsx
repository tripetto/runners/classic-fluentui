import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Dropdown } from "@tripetto/block-dropdown/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { DropdownControl } from "@ui/controls/dropdown";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-dropdown",
})
export class DropdownBlock extends Dropdown implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <DropdownControl
                    id={props.id}
                    label={props.fieldLabel}
                    placeholder={props.placeholder}
                    options={this.options}
                    value={this.dropdownSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
