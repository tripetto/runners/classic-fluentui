import { ReactNode } from "react";
import { Num, castToString, isArray, isNumberFinite, tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Scale } from "@tripetto/block-scale/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { Stack } from "@fluentui/react/lib/Stack";
import { DefaultButton, PrimaryButton } from "@fluentui/react/lib/Button";
import { Text } from "@fluentui/react/lib/Text";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-scale",
})
export class ScaleBlock extends Scale implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        const buttons: JSX.Element[] = [];

        if (isArray(this.options)) {
            buttons.push(
                ...this.options.map((option, index) => (
                    <Button
                        key={option.id || index}
                        selected={this.scaleSlot.reference === option.id}
                        disabled={this.scaleSlot.isLocked || this.scaleSlot.isFrozen}
                        label={option.name || "..."}
                        justify={this.props.justify || false}
                        tabIndex={props.tabIndex}
                        onSelect={() => this.scaleSlot.set(option.value || option.name, option.id)}
                        onDeselect={() => !this.required && this.scaleSlot.clear()}
                    />
                ))
            );
        } else {
            const from = Num.min(this.options.from, this.options.to);
            const to = Num.max(this.options.from, this.options.to);
            const stepSize = Num.max(this.options.stepSize || 0, 1);

            if (!isNumberFinite(this.scaleSlot.pristine)) {
                this.scaleSlot.clear();
            }

            for (let index = from; index <= to; index += stepSize) {
                buttons.push(
                    <Button
                        key={index}
                        selected={this.scaleSlot.pristine === index}
                        disabled={this.scaleSlot.isLocked || this.scaleSlot.isFrozen}
                        label={castToString(index)}
                        justify={this.props.justify || false}
                        tabIndex={props.tabIndex}
                        onSelect={() => (this.scaleSlot.value = index)}
                        onDeselect={() => !this.required && this.scaleSlot.clear()}
                    />
                );
            }
        }

        return (
            <>
                {props.label}
                {props.description}
                <Stack horizontal wrap tokens={{ childrenGap: 4 }}>
                    {buttons}
                </Stack>
                {buttons.length > 0 &&
                    (this.props.labelLeft ||
                        (this.props.labelCenter && buttons.length > 2) ||
                        (this.props.labelRight && buttons.length > 1)) && (
                        <Stack horizontal horizontalAlign="space-between" tokens={{ childrenGap: 4 }}>
                            <Stack.Item>
                                <Text variant="small">{this.props.labelLeft || ""}</Text>
                            </Stack.Item>
                            <Stack.Item>
                                <Text variant="small">{(buttons.length > 2 && this.props.labelCenter) || ""}</Text>
                            </Stack.Item>
                            <Stack.Item>
                                <Text variant="small">{(buttons.length > 1 && this.props.labelRight) || ""}</Text>
                            </Stack.Item>
                        </Stack>
                    )}
                {props.ariaDescription}
            </>
        );
    }
}

const Button = (props: {
    readonly selected: boolean;
    readonly disabled: boolean;
    readonly tabIndex: number;
    readonly label: string;
    readonly justify: boolean;
    readonly onSelect: () => void;
    readonly onDeselect: () => void;
}) =>
    props.selected ? (
        <PrimaryButton
            disabled={props.disabled}
            tabIndex={props.tabIndex}
            text={props.label}
            onClick={props.onDeselect}
            style={{ minWidth: "auto", flexGrow: (props.justify && 1) || undefined }}
        />
    ) : (
        <DefaultButton
            disabled={props.disabled}
            tabIndex={props.tabIndex}
            text={props.label}
            onClick={props.onSelect}
            style={{ minWidth: "auto", flexGrow: (props.justify && 1) || undefined }}
        />
    );
