import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { MultiSelect } from "@tripetto/block-multi-select/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { MultiSelectControl } from "@ui/controls/multi-select";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-multi-select",
})
export class MultiSelectBlock extends MultiSelect implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <MultiSelectControl
                    id={props.id}
                    label={props.fieldLabel}
                    placeholder={props.placeholder}
                    options={this.options(props)}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
