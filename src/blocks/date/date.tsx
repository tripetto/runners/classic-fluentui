import { ReactNode } from "react";
import { Value, tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { DateTime } from "@tripetto/block-date/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { DatePicker, defaultDatePickerStrings } from "@fluentui/react/lib/DatePicker";
import { TimePicker } from "@fluentui/react/lib/TimePicker";
import { Stack } from "@fluentui/react/lib/Stack";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-date",
})
export class DateTimeBlock extends DateTime implements IClassicRendering {
    private timePickerKey = 0;
    readonly useLabelAsTitle = true;

    private updateDate(date: Date | undefined, ref: Value<number>): void {
        if (date) {
            const current = new Date(ref.value);

            current.setUTCFullYear(date.getFullYear());
            current.setUTCMonth(date.getMonth());
            current.setUTCDate(date.getDate());

            if (!this.props.time || !ref.hasValue) {
                current.setUTCHours(0);
                current.setUTCMinutes(0);
                current.setUTCSeconds(0);
                current.setUTCMilliseconds(0);
            }

            ref.set(current.getTime());
        } else {
            ref.clear();
        }
    }

    private updateTime(time: Date, ref: Value<number>): void {
        const current = ref.hasValue ? new Date(ref.value) : new Date();

        current.setUTCHours(time.getHours());
        current.setUTCMinutes(time.getMinutes());
        current.setUTCSeconds(0);
        current.setUTCMilliseconds(0);

        ref.set(current.getTime());
    }

    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <Stack horizontal wrap tokens={{ childrenGap: 12 }}>
                    <Stack.Item
                        grow
                        style={{
                            minWidth: 150,
                            width: (!this.props.time && this.props.range && "calc(50% - 12px)") || undefined,
                        }}
                    >
                        <DatePicker
                            placeholder={props.placeholder}
                            ariaLabel={props.name}
                            tabIndex={props.tabIndex}
                            strings={defaultDatePickerStrings}
                            isRequired={!props.name && !this.props.time && !this.props.range && this.required}
                            formatDate={(date) => props.l10n.locale.dateFull(date ? date.getTime() : this.dateSlot.value)}
                            value={(this.dateSlot.hasValue && new Date(this.dateSlot.value)) || undefined}
                            onSelectDate={(date) => this.updateDate(date as Date | undefined, this.dateSlot)}
                            disabled={this.dateSlot.isFrozen || this.dateSlot.isLocked}
                        />
                    </Stack.Item>
                    {this.props.time && (
                        <Stack.Item
                            grow
                            style={{
                                minWidth: 120,
                                maxWidth: 150,
                            }}
                        >
                            <TimePicker
                                useComboBoxAsMenuWidth
                                allowFreeform={false}
                                onFormatDate={(date) => props.l10n.locale.timeFull(date.getTime())}
                                // TimePicker doesn't support the value prop, so we need this hack to assure the value of the slot is applied.
                                key={this.timePickerKey++}
                                defaultValue={
                                    new Date(this.dateSlot.hasValue ? this.dateSlot.value + new Date().getTimezoneOffset() * 60 * 1000 : 0)
                                }
                                onChange={(e, time) => this.updateTime(time, this.dateSlot)}
                                disabled={this.dateSlot.isFrozen || this.dateSlot.isLocked}
                            />
                        </Stack.Item>
                    )}
                    {this.props.range && this.toSlot && (
                        <>
                            <Stack.Item
                                grow
                                style={{
                                    minWidth: 150,
                                    width: (!this.props.time && "calc(50% - 12px)") || undefined,
                                }}
                            >
                                <DatePicker
                                    placeholder={this.toPlaceholder}
                                    ariaLabel={props.name}
                                    tabIndex={props.tabIndex}
                                    strings={defaultDatePickerStrings}
                                    formatDate={(date) => props.l10n.locale.dateFull(date ? date.getTime() : this.dateSlot.value)}
                                    value={(this.toSlot?.hasValue && new Date(this.toSlot.value)) || undefined}
                                    onSelectDate={(date) => this.updateDate(date as Date | undefined, this.toSlot!)}
                                    disabled={this.toSlot.isFrozen || this.toSlot.isLocked}
                                />
                            </Stack.Item>
                            {this.props.time && (
                                <Stack.Item
                                    grow
                                    style={{
                                        minWidth: 120,
                                        maxWidth: 150,
                                    }}
                                >
                                    <TimePicker
                                        useComboBoxAsMenuWidth
                                        allowFreeform={false}
                                        onFormatDate={(date) => props.l10n.locale.timeFull(date.getTime())}
                                        // TimePicker doesn't support the value prop, so we need this hack to assure the value of the slot is applied.
                                        key={this.timePickerKey++}
                                        defaultValue={
                                            new Date(
                                                this.toSlot.hasValue ? this.toSlot.value + new Date().getTimezoneOffset() * 60 * 1000 : 0
                                            )
                                        }
                                        onChange={(e, time) => this.updateTime(time, this.toSlot!)}
                                        disabled={this.toSlot.isFrozen || this.toSlot.isLocked}
                                    />
                                </Stack.Item>
                            )}
                        </>
                    )}
                </Stack>
                {props.ariaDescription}
            </>
        );
    }
}
