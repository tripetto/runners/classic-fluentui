import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Number } from "@tripetto/block-number/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { NumberControl } from "@ui/controls/number";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-number",
})
export class NumberBlock extends Number implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <NumberControl
                    id={props.id}
                    l10n={props.l10n}
                    label={props.fieldLabel}
                    placeholder={props.placeholder}
                    explanation={props.explanation}
                    value={this.numberSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
            </>
        );
    }
}
