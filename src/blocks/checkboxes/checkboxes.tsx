import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Checkboxes } from "@tripetto/block-checkboxes/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { CheckboxControl } from "@ui/controls/checkbox";
import { Stack } from "@fluentui/react/lib/Stack";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-checkboxes",
})
export class CheckboxesBlock extends Checkboxes implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        const checkboxes = this.checkboxes(props);

        return (
            <>
                {props.label}
                {props.description}
                <Stack tokens={{ childrenGap: 14 }} style={{ paddingTop: "8px" }}>
                    {checkboxes.map(
                        (checkbox, index) =>
                            (props.view === "preview" || checkbox.label) && (
                                <CheckboxControl
                                    key={checkbox.id || index}
                                    label={() =>
                                        checkbox.description && checkbox.label ? (
                                            <>
                                                <b>{checkbox.label}</b>
                                                <br />
                                                {checkbox.description}
                                            </>
                                        ) : (
                                            checkbox.label || checkbox.description
                                        )
                                    }
                                    tabIndex={checkbox.tabIndex || props.tabIndex}
                                    tabSubmit={index + 1 === checkboxes.length}
                                    value={checkbox.value!}
                                    ariaDescribedBy={props.ariaDescribedBy}
                                    onAutoFocus={(index === 0 && props.autoFocus) || undefined}
                                    onFocus={props.focus}
                                    onBlur={props.blur}
                                    onSubmit={props.onSubmit}
                                />
                            )
                    )}
                </Stack>
                {props.ariaDescription}
            </>
        );
    }
}
