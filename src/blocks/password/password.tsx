import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Password } from "@tripetto/block-password/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { InputControl } from "@ui/controls/input";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-password",
})
export class PasswordBlock extends Password implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <InputControl
                    id={props.id}
                    type="password"
                    autoComplete="new-password"
                    label={props.fieldLabel}
                    explanation={props.explanation}
                    value={this.passwordSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
            </>
        );
    }
}
