import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { MultipleChoice } from "@tripetto/block-multiple-choice/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { Text } from "@fluentui/react/lib/Text";
import { Stack } from "@fluentui/react/lib/Stack";
import { CompoundButton, DefaultButton, PrimaryButton } from "@fluentui/react/lib/Button";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-multiple-choice",
    alias: "multiple-choice",
    autoRender: true,
})
export class MultipleChoiceBlock extends MultipleChoice implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.caption ? (
                    <>
                        <Text block={true} variant="xxLarge" style={{ marginBottom: "8px" }}>
                            {props.title}
                        </Text>
                        <Text block={true} variant="xLarge">
                            {props.markdownifyToJSX(this.props.caption)}
                        </Text>
                    </>
                ) : (
                    props.label
                )}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <Stack
                    horizontal={this.props.alignment === true || this.props.alignment === "columns"}
                    wrap
                    tokens={{ childrenGap: 12 }}
                    style={{
                        display: "table",
                        width: this.props.alignment === "equal" ? "auto" : "100%",
                    }}
                >
                    {this.choices(props).map((button, index) => {
                        if (button.slot) {
                            button.slot.confirm();
                        }

                        return (
                            <Button
                                key={button.id || index}
                                selected={button.slot ? button.slot.value : this.singleChoiceSlot?.reference === button.id}
                                alignment={
                                    (this.props.alignment === "equal" ||
                                    this.props.alignment === "full" ||
                                    this.props.alignment === "columns"
                                        ? this.props.alignment
                                        : this.props.alignment && "horizontal") || "vertical"
                                }
                                tabIndex={props.tabIndex}
                                label={button.name || "..."}
                                description={button.descriptionString}
                                color={button.color}
                                disabled={
                                    button.disabled ||
                                    button.slot?.isLocked ||
                                    button.slot?.isFrozen ||
                                    this.singleChoiceSlot?.isLocked ||
                                    this.singleChoiceSlot?.isFrozen ||
                                    false
                                }
                                href={button.url}
                                target={button.target}
                                onSelect={
                                    (!button.url &&
                                        (() => {
                                            if (button.slot) {
                                                button.slot.value = true;
                                            } else if (this.singleChoiceSlot) {
                                                this.singleChoiceSlot.set(button.value || button.name, button.id);
                                            }
                                        })) ||
                                    undefined
                                }
                                onDeselect={
                                    (!button.url &&
                                        (() => {
                                            if (button.slot) {
                                                button.slot.value = false;
                                            } else if (this.singleChoiceSlot) {
                                                this.singleChoiceSlot.clear();
                                            }
                                        })) ||
                                    undefined
                                }
                            />
                        );
                    })}
                </Stack>
                {props.ariaDescription}
            </>
        );
    }
}

const Button = (props: {
    readonly alignment: "vertical" | "equal" | "full" | "columns" | "horizontal";
    readonly selected: boolean;
    readonly disabled: boolean;
    readonly tabIndex: number;
    readonly label: string;
    readonly description?: string;
    readonly color?: string;
    readonly href?: string;
    readonly target?: "self" | "blank";
    readonly onSelect?: () => void;
    readonly onDeselect?: () => void;
}) => {
    const style = {
        display: props.alignment === "vertical" || props.alignment === "equal" || props.alignment === "full" ? "block" : "inline-block",
        width:
            ((props.alignment === "equal" || props.alignment === "full") && "100%") ||
            (props.alignment === "columns" && "calc(50% - 12px)") ||
            "auto",
        alignSelf: (props.alignment === "vertical" && "start") || undefined,
        backgroundColor: (props.selected && props.color) || undefined,
    };

    return props.description ? (
        <CompoundButton
            tabIndex={props.tabIndex}
            disabled={props.disabled}
            primary={props.selected}
            text={props.label}
            secondaryText={props.description}
            onClick={
                props.href
                    ? (e) => {
                          e.stopPropagation();

                          window.open(props.href, `_${props.target || "blank"}`, "noopener");
                      }
                    : props.selected
                    ? props.onDeselect
                    : props.onSelect
            }
            style={style}
        />
    ) : props.selected ? (
        <PrimaryButton tabIndex={props.tabIndex} disabled={props.disabled} text={props.label} onClick={props.onDeselect} style={style} />
    ) : (
        <DefaultButton
            tabIndex={props.tabIndex}
            disabled={props.disabled}
            text={props.label}
            onClick={
                props.href
                    ? (e) => {
                          e.stopPropagation();

                          window.open(props.href, `_${props.target || "blank"}`, "noopener");
                      }
                    : props.onSelect
            }
            style={style}
        />
    );
};
