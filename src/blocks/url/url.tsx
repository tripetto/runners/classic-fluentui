import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { URL } from "@tripetto/block-url/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { InputControl } from "@ui/controls/input";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-url",
})
export class URLBlock extends URL implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <InputControl
                    id={props.id}
                    type="url"
                    inputMode="url"
                    label={props.fieldLabel}
                    explanation={props.explanation}
                    value={this.urlSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder || "https://"}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
            </>
        );
    }
}
