import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Text } from "@tripetto/block-text/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { TextControl } from "@ui/controls/text";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-text",
})
export class TextBlock extends Text implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <TextControl
                    id={props.id}
                    label={props.fieldLabel}
                    placeholder={props.placeholder}
                    explanation={props.explanation}
                    value={this.textSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    maxLength={this.maxLength}
                    autoComplete={this.autoComplete}
                    suggestions={this.suggestions}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {this.suggestions && (
                    <datalist id={props.id + "_list"}>
                        {this.suggestions.map((suggestion, index) => suggestion && <option key={index} value={suggestion} />)}
                    </datalist>
                )}
            </>
        );
    }
}
