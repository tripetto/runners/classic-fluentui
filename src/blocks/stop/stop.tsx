import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Stop } from "@tripetto/block-stop/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { Text } from "@fluentui/react/lib/Text";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-stop",
})
export class StopBlock extends Stop implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            (props.title || props.description || this.props.imageURL) && (
                <>
                    {this.props.imageURL && this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                    {props.title && (
                        <Text block={true} variant="xxLarge" style={{ marginBottom: "8px" }}>
                            {props.title}
                        </Text>
                    )}
                    {props.description}
                    {this.props.imageURL && !this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                </>
            )
        );
    }
}
