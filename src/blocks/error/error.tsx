import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { markdownifyToJSX } from "@tripetto/runner-react-hook";
import { Error } from "@tripetto/block-error/runner";
import { namespace } from "@namespace";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { MessageBar, MessageBarType } from "@fluentui/react/lib/MessageBar";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-error",
})
export class ErrorBlock extends Error implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        const description = this.node.description || "";

        return (
            (props.name || description) && (
                <MessageBar delayedRender={false} messageBarType={MessageBarType.warning}>
                    {props.name && description ? (
                        <>
                            <b>{markdownifyToJSX(props.name, this.context)}</b>
                            <br />
                            {markdownifyToJSX(description, this.context)}
                        </>
                    ) : (
                        markdownifyToJSX(props.name || description, this.context)
                    )}
                </MessageBar>
            )
        );
    }
}
