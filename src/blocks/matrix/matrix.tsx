import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Matrix } from "@tripetto/block-matrix/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { MatrixControl } from "@ui/controls/matrix";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-matrix",
})
export class MatrixBlock extends Matrix implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <MatrixControl
                    columns={this.columns(props)}
                    rows={this.rows(props)}
                    ariaDescribedBy={props.ariaDescribedBy}
                    allowUnselect={true}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
