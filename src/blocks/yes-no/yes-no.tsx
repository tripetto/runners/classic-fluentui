import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { YesNo } from "@tripetto/block-yes-no/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { Stack } from "@fluentui/react/lib/Stack";
import { DefaultButton, PrimaryButton } from "@fluentui/react/lib/Button";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-yes-no",
    alias: "yes-no",
})
export class YesNoBlock extends YesNo implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        const yesLabel = props.markdownifyToString(this.props.altYes || "") || props.l10n.pgettext("runner#6|🔷 Yes/No", "Yes");
        const noLabel = props.markdownifyToString(this.props.altNo || "") || props.l10n.pgettext("runner#6|🔷 Yes/No", "No");

        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToURL(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.label}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToURL(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <Stack horizontal tokens={{ childrenGap: 12 }}>
                    <Button
                        selected={this.answerSlot.reference === "yes"}
                        disabled={this.answerSlot.isLocked || this.answerSlot.isFrozen}
                        tabIndex={props.tabIndex}
                        label={yesLabel}
                        color={this.props.colorYes}
                        icon="Accept"
                        onSelect={() => this.answerSlot.set(yesLabel, "yes")}
                        onDeselect={() => this.answerSlot.clear()}
                    />
                    <Button
                        selected={this.answerSlot.reference === "no"}
                        disabled={this.answerSlot.isLocked || this.answerSlot.isFrozen}
                        tabIndex={props.tabIndex}
                        label={noLabel}
                        color={this.props.colorNo}
                        icon="Cancel"
                        onSelect={() => this.answerSlot.set(noLabel, "no")}
                        onDeselect={() => this.answerSlot.clear()}
                    />
                </Stack>
                {props.ariaDescription}
            </>
        );
    }
}

const Button = (props: {
    readonly selected: boolean;
    readonly disabled: boolean;
    readonly tabIndex: number;
    readonly label: string;
    readonly color?: string;
    readonly icon: string;
    readonly onSelect: () => void;
    readonly onDeselect: () => void;
}) =>
    props.selected ? (
        <PrimaryButton
            tabIndex={props.tabIndex}
            text={props.label}
            iconProps={{ iconName: props.icon }}
            disabled={props.disabled}
            onClick={props.onDeselect}
            style={{
                backgroundColor: props.color,
            }}
        />
    ) : (
        <DefaultButton
            tabIndex={props.tabIndex}
            text={props.label}
            iconProps={{ iconName: props.icon }}
            disabled={props.disabled}
            onClick={props.onSelect}
        />
    );
