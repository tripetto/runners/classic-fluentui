import { styled } from "styled-components";
import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Statement } from "@tripetto/block-statement/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { Text } from "@fluentui/react/lib/Text";
import { FontIcon } from "@fluentui/react/lib/Icon";

const StatementElement = styled.div`
    position: relative;
    padding-left: 32px;

    > i:first-child {
        position: absolute;
        left: 0;
        top: 9px;
        font-size: 42px;
    }

    span + span {
        margin-top: 8px;
    }
`;

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-statement",
    alias: "statement",
})
export class StatementBlock extends Statement implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                <StatementElement>
                    <FontIcon iconName="RightDoubleQuote" />
                    {this.props.imageURL && this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                    <Text block={true} variant="xxLarge">
                        {props.title}
                    </Text>
                    {props.description}
                    {this.props.imageURL && !this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                    {props.ariaDescription}
                </StatementElement>
            </>
        );
    }
}
