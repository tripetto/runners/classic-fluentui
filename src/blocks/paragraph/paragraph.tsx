import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Paragraph } from "@tripetto/block-paragraph/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { BlockVideo } from "@ui/blocks/video";
import { Text } from "@fluentui/react/lib/Text";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-paragraph",
    alias: "paragraph",
})
export class ParagraphBlock extends Paragraph implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.title && (
                    <Text block={true} variant="xxLarge" style={{ marginBottom: this.props.caption ? "4px" : "8px" }}>
                        {props.title}
                    </Text>
                )}
                {this.props.caption && (
                    <Text block={true} variant="xLarge" style={{ marginBottom: "8px" }}>
                        {props.markdownifyToJSX(this.props.caption)}
                    </Text>
                )}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.video && <BlockVideo src={props.markdownifyToURL(this.props.video)} view={props.view} />}
                {props.ariaDescription}
            </>
        );
    }
}
