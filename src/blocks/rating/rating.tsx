import { ReactNode } from "react";
import { Num, tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Rating } from "@tripetto/block-rating/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { Stack } from "@fluentui/react/lib/Stack";
import { IconButton } from "@fluentui/react/lib/Button";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-rating",
    alias: "rating",
})
export class RatingBlock extends Rating implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        const buttons: ReactNode[] = [];
        const steps = Num.max(1, this.steps);
        const disabled = this.ratingSlot.isLocked || this.ratingSlot.isFrozen;
        const iconName =
            this.props.shape === "hearts"
                ? "Heart"
                : this.props.shape === "thumbs-up"
                ? "Emoji2"
                : this.props.shape === "thumbs-down"
                ? "EmojiDisappointed"
                : this.props.shape === "persons"
                ? "UserFollowed"
                : "FavoriteStar";

        for (let i = 1; i <= steps; i++) {
            const checked = this.ratingSlot.value >= i;

            buttons.push(
                <IconButton
                    key={i}
                    iconProps={{ iconName: iconName + (checked && (iconName === "Heart" || iconName === "FavoriteStar") ? "Fill" : "") }}
                    checked={checked}
                    disabled={disabled}
                    onClick={() => {
                        this.ratingSlot.value = checked && this.ratingSlot.value === i ? i - 1 : i;

                        if (!this.ratingSlot.value) {
                            this.ratingSlot.clear();
                        }
                    }}
                />
            );
        }
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.label}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <Stack horizontal tokens={{ childrenGap: -2 }}>
                    {buttons}
                </Stack>
                {props.ariaDescription}
            </>
        );
    }
}
