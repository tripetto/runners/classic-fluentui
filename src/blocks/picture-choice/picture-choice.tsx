import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { PictureChoice } from "@tripetto/block-picture-choice/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { PictureChoiceControl } from "@ui/controls/picture-choice";
import { Text } from "@fluentui/react/lib/Text";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-picture-choice",
    autoRender: true,
})
export class PictureChoiceBlock extends PictureChoice implements IClassicRendering {
    readonly autoSubmit = !this.props.multiple;
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.caption ? (
                    <>
                        <Text block={true} variant="xxLarge" style={{ marginBottom: "8px" }}>
                            {props.title}
                        </Text>
                        <Text block={true} variant="xLarge">
                            {props.markdownifyToJSX(this.props.caption)}
                        </Text>
                    </>
                ) : (
                    props.label
                )}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <PictureChoiceControl
                    view={props.view}
                    options={this.choices(props)}
                    size={this.props.size}
                    value={(!this.props.multiple && this.valueOf("choice")) || undefined}
                    required={this.required}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
                {props.ariaDescription}
            </>
        );
    }
}
