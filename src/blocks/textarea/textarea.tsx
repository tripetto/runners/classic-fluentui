import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Textarea } from "@tripetto/block-textarea/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { InputControl } from "@ui/controls/input";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-textarea",
})
export class TextareaBlock extends Textarea implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <InputControl
                    id={props.id}
                    type={"text"}
                    multiline={true}
                    label={props.fieldLabel}
                    placeholder={props.placeholder}
                    explanation={props.explanation}
                    value={this.textareaSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    maxLength={this.maxLength}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
            </>
        );
    }
}
