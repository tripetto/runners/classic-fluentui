import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { PhoneNumber } from "@tripetto/block-phone-number/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { InputControl } from "@ui/controls/input";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-phone-number",
})
export class PhoneNumberBlock extends PhoneNumber implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <InputControl
                    id={props.id}
                    type="tel"
                    inputMode="tel"
                    autoComplete="tel"
                    label={props.fieldLabel}
                    explanation={props.explanation}
                    value={this.phoneNumberSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder || "+1 (201) 555-5555"}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
            </>
        );
    }
}
