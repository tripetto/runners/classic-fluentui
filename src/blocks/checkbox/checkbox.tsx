import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Checkbox } from "@tripetto/block-checkbox/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { CheckboxControl } from "@ui/controls/checkbox";
import { Stack } from "@fluentui/react/lib/Stack";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-checkbox",
    alias: "checkbox",
})
export class CheckboxBlock extends Checkbox implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <Stack style={{ paddingTop: "8px" }}>
                    <CheckboxControl
                        value={this.checkboxSlot}
                        required={this.required}
                        error={props.isFailed}
                        label={props.fieldLabel}
                        tabIndex={props.tabIndex}
                        ariaDescribedBy={props.ariaDescribedBy}
                        onAutoFocus={props.autoFocus}
                        onFocus={props.focus}
                        onBlur={props.blur}
                        onSubmit={props.onSubmit}
                    />
                </Stack>
                {props.ariaDescription}
            </>
        );
    }
}
