import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Radiobuttons } from "@tripetto/block-radiobuttons/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { RadiobuttonsControl } from "@ui/controls/radiobuttons";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-radiobuttons",
})
export class RadiobuttonsBlock extends Radiobuttons implements IClassicRendering {
    readonly useLabelAsTitle = true;

    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <RadiobuttonsControl
                    buttons={this.buttons(props)}
                    value={this.radioSlot}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
