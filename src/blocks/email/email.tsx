import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Email } from "@tripetto/block-email/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { InputControl } from "@ui/controls/input";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-email",
})
export class EmailBlock extends Email implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.label}
                {props.description}
                <InputControl
                    id={props.id}
                    type="email"
                    inputMode="email"
                    autoComplete="email"
                    label={props.fieldLabel}
                    explanation={props.explanation}
                    value={this.emailSlot}
                    required={!props.name && this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder || "@"}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
            </>
        );
    }
}
