import { NgModule } from "@angular/core";
import { TripettoClassicComponent } from "./runner.component";

@NgModule({
    declarations: [TripettoClassicComponent],
    imports: [],
    exports: [TripettoClassicComponent],
})
export class TripettoClassicModule {}
