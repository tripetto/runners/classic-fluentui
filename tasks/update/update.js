const fs = require("fs");
const pkg = require("../../package.json");
const synchronizedPrettier = require("@prettier/sync");

pkg.peerDependencies["react"] = ">= 16.14.x";
pkg.peerDependencies["react-dom"] = ">= 16.x";
pkg.peerDependencies["@tripetto/runner"] = ">= 8.x";
pkg.peerDependencies["@types/react"] = ">= 16.x";

fs.writeFileSync("./package.json", synchronizedPrettier.format(JSON.stringify(pkg), { parser: "json-stringify" }), "utf8");
