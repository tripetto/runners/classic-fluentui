import TripettoClassicAsDefaultImport from "../runner/esm/index.mjs";
import { TripettoClassic, ClassicRunner } from "../runner/esm/index.mjs";
import "../builder/esm/index.mjs";

try {
    if (typeof TripettoClassicAsDefaultImport.ClassicRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }

    if (typeof TripettoClassic.ClassicRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }

    if (typeof ClassicRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }
} catch(e) {
    throw new Error("ES6 module failed!");
}
